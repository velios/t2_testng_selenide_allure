package core;

import javax.swing.*;

public class UIElementsHelper {

    static String msg_box_input(String message) {
        return JOptionPane.showInputDialog(
                null,
                message
        );
    }

    public static int msg_box_yes_no(String message) {
        int dialogButton = JOptionPane.YES_NO_OPTION;
        return JOptionPane.showConfirmDialog(
                null,
                message,
                null,
                dialogButton);
    }

    public static String msg_box_listbox(String message, String[] choices, int number_of_initial_choice) {
        return (String) JOptionPane.showInputDialog(
                null,
                message,
                null, JOptionPane.QUESTION_MESSAGE,
                null,
                choices,
                choices[number_of_initial_choice]);
    }

}
