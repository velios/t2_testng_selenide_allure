package core;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static core.ApplicationManager.collect_t2_version;
import static core.ApplicationManager.site_version;
import static test.TestBase.cfg;

class AuthHelper {
    
    @Step("Авторизация внутреннего пользователя {login}")
    void login_as_system_user(String login) {
        open(cfg.internal_base_url());
        $(byText("Служебный вход")).shouldBe(visible).click();
        $(byText("Войти")).shouldBe(visible);
        if ($(byAttribute("placeholder", "Имя пользователя")).isDisplayed()) {
            $(byAttribute("placeholder", "Имя пользователя")).setValue(login);
            $(byAttribute("placeholder", "Пароль")).setValue(cfg.internal_user_password());
            $(byText("Войти")).click();
            $("title").shouldHave(attribute("text", "Рабочий стол для сотрудников Maykor"));
    
            site_version = collect_t2_version();
        } else {
            System.out.println("Повторная авторизация системного пользователя не потребовалась");
        }
    }
    
    @Step("Авторизация инженера {login}")
    void login_as_engineer(String login) {
        open(cfg.external_base_url());
        $(byText("Личный кабинет")).shouldBe(visible).click();
        $(byText("Войти")).shouldBe(visible);
        if ($(byAttribute("placeholder", "Имя пользователя")).isDisplayed()) {
            $(byAttribute("placeholder", "Имя пользователя")).setValue(login);
            $(byAttribute("placeholder", "Пароль")).setValue(cfg.external_user_password());
            $(byText("Войти")).click();
            $("title").shouldHave(attribute("text", "Рабочий стол инженера"));
    
            site_version = collect_t2_version();
        } else {
            System.out.println("Повторная авторизация инженера не потребовалась");
        }
    }
}
