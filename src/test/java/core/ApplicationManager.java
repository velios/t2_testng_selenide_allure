package core;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import com.jamesmurty.utils.XMLBuilder;
import io.qameta.allure.Step;
import javafx.util.Pair;
import lombok.Getter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.SkipException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import static com.codeborne.selenide.Configuration.FileDownloadMode.PROXY;
import static com.codeborne.selenide.Configuration.SelectorMode.Sizzle;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static core.SSHConnectionHelper.add_log_result_to_allure;
import static test.TestBase.cfg;

public class ApplicationManager {
    static Boolean is_error_msg = false;
    static String site_version = "";
    
    private static SSHConnectionHelper internal_ssh_connection;
    private static SSHConnectionHelper external_ssh_connection;
    @Getter
    private AuthHelper authHelper = new AuthHelper();
    
    public static SelenideElement return_right_element_by_cell_title(String cell_title) {
        SelenideElement element;
        element = $(byText(cell_title))
                          .parent()
                          .parent()
                          .findAll("td")
                          .last()
                          .find("*");
        return element;
    }
    
    public static void disable_animation() {
        //Отключение JQuery анимации. Если включить некоторые функции не работают
        executeJavaScript("$.fx.off = true;");
    }
    
    public void suite_init() {
        System.setProperty("file.encoding", "UTF-8");
    
        //Решение проблемы неподписанных сертификатов
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setAcceptInsecureCerts(true);
        Configuration.browserCapabilities = capabilities;
        
        Configuration.browser = cfg.browser();
        Configuration.headless = cfg.browser_headless();
        Configuration.browserSize=cfg.browser_size();
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;

//      Совет из http://ru.selenide.org/2018/04/25/selenide-4.11.2/
        Configuration.setValueChangeEvent = false;
        Configuration.fileDownload = PROXY;
        Configuration.selectorMode = Sizzle;

        Configuration.holdBrowserOpen = cfg.browser_hold_open();
    }
    
    public void method_init() {
        if (cfg.is_analyze_remote_logs()) {
            is_error_msg = false;
            
            if (internal_ssh_connection == null) {
                internal_ssh_connection = new SSHConnectionHelper(
                        cfg.internal_ssh_host(),
                        cfg.internal_ssh_user_login(),
                        cfg.internal_ssh_key(),
                        "tail -n 0 -f /opt/wildfly/standalone/log/server.log");
            }
            internal_ssh_connection.start_thread();
            
            if (external_ssh_connection == null) {
                external_ssh_connection = new SSHConnectionHelper(
                        cfg.external_ssh_host(),
                        cfg.external_ssh_user_login(),
                        cfg.external_ssh_key(),
                        "tail -n 0 -f /opt/wildfly/standalone/log/server.log");
            }
            external_ssh_connection.start_thread();
        }
    }
    
    public void method_stop() throws ExecutionException, InterruptedException {
        if (cfg.is_analyze_remote_logs()) {
            Pair<Boolean, String> internal_log_result = internal_ssh_connection.get_thread_result();
            add_log_result_to_allure(internal_log_result, "Internal log");
            
            Pair<Boolean, String> external_log_result = external_ssh_connection.get_thread_result();
            add_log_result_to_allure(external_log_result, "External log");
        }
    }
    
    public void suite_stop() throws IOException {
        collect_and_store_allure_properties_file();
        if (cfg.is_analyze_remote_logs()) {
            internal_ssh_connection.close();
            external_ssh_connection.close();
        }
    }
    
    public void system_user_tests_init() {
        authHelper.login_as_system_user(cfg.internal_user_login());
    }
    
    public void engineer_tests_init() {
        authHelper.login_as_engineer(cfg.external_user_login());
    }
    
    @Step("Еще не реализовано. Запланированная остановка теста.")
    public static void not_implemented_dummy() {
        throw new SkipException("Еще не реализовано. Запланированная остановка теста.");
    }
    
    @Step("Блокировано. Невозможно реализовать дальше.")
    public static void blocked_dummy(String message) {
        throw new RuntimeException(message);
    }
    
    private void collect_and_store_allure_properties_file() {
        try {
            new File(".//target//allure-results").mkdirs();
            XMLBuilder builder =
                    XMLBuilder.create("environment")
                            .e("parameter")
                            .e("key").t("Сервер системного пользователя").up()
                            .e("value").t(cfg.internal_ssh_host()).up().up()
                            .e("parameter")
                            .e("key").t("Сервер инженера").up()
                            .e("value").t(cfg.external_ssh_host()).up().up()
                            .e("parameter")
                            .e("key").t("Инженер").up()
                            .e("value").t(cfg.external_user_login()).up().up()
                            .e("parameter")
                            .e("key").t("Системный пользователь").up()
                            .e("value").t(cfg.internal_user_login()).up().up()
                            .e("parameter")
                            .e("key").t("Номер стенда").up()
                            .e("value").t(String.format("Стенд %s. %s", cfg.stand_number(), site_version));
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(
                    new FileOutputStream(".//target//allure-results//environment.xml"), StandardCharsets.UTF_8), true);
            Properties outputProperties = new Properties();
            // Omit the XML declaration header
            outputProperties.put(
                    javax.xml.transform.OutputKeys.OMIT_XML_DECLARATION, "yes");
            builder.toWriter(writer, outputProperties);
        } catch (ParserConfigurationException | FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
        
    }
    
    static String collect_t2_version() {
        return $("div.footer").find("span.footer-text-left").getText();
    }
}
