package core;

import io.qameta.allure.Allure;
import javafx.util.Pair;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import net.schmizz.sshj.userauth.keyprovider.KeyProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.*;

class SSHConnectionHelper {

    private BufferedReader buffered_reader;

    private SSHClient ssh;
    private Session session;
    private Session.Command cmd;

    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private Future<Pair<Boolean, String>> ssh_reader_future;
    private SSHConnectionCallable ssh_reader_thread;

    SSHConnectionHelper(String host, String user, String key_path, String shell_command) {
        ssh = new SSHClient();

        ssh.addHostKeyVerifier(new PromiscuousVerifier());
        KeyProvider keys;
        try {
            keys = ssh.loadKeys(key_path);
            ssh.connect(host);
            ssh.authPublickey(user, keys);
        
            session = ssh.startSession();
            session.allocateDefaultPTY();
        
            cmd = session.exec(shell_command);
        
        } catch (IOException e) {
            System.out.println("Не найден открытый ключ для доступа к ssh");
            e.printStackTrace();
        }

        if (cmd != null) {
            buffered_reader = new BufferedReader(new InputStreamReader(cmd.getInputStream()));
        }

    }


    void start_thread() {
        ssh_reader_thread = new SSHConnectionCallable(buffered_reader);
        ssh_reader_future = executor.submit(ssh_reader_thread);
    }


    Pair<Boolean, String> get_thread_result() throws ExecutionException, InterruptedException {
        ssh_reader_thread.finish();
        return ssh_reader_future.get();
    }

    static void add_log_result_to_allure(Pair<Boolean, String> log_result, String log_name) {
        log_name = log_name == null ? "Log" : log_name;
        Boolean is_error_in_logs = log_result.getKey();
        String attachment_name = is_error_in_logs ? "[ERROR] " + log_name : "[Normal] " + log_name;
        String log_result_text = log_result.getValue();
        if (!log_result_text.isEmpty()) {
            Allure.addAttachment(attachment_name, log_result_text);
        }
    }

    void close() throws IOException {
        cmd.getOutputStream().write(3);
        cmd.getOutputStream().flush();
        cmd.join(2, TimeUnit.SECONDS);
        buffered_reader.close();
        if (session != null) {
            session.close();
        }
        ssh.disconnect();
        executor.shutdown();
        try {
            if (!executor.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executor.shutdownNow(); // Cancel currently executing tasks
            }
        } catch (InterruptedException ie) {
            executor.shutdownNow();
        }
    }
}
