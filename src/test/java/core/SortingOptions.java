package core;

public enum SortingOptions {
    ASCENDING,
    DESCENDING,
    UNSORTED
}
