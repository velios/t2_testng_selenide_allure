package core;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.util.concurrent.Callable;

public class SSHConnectionCallable implements Callable<Pair<Boolean, String>> {

    private Boolean isFinish = false;

    private final BufferedReader buffered_reader;

    SSHConnectionCallable(BufferedReader buffered_reader) {
        this.buffered_reader = buffered_reader;
    }

    void finish() {
        isFinish = true;
    }

    @Override
    public Pair<Boolean, String> call() throws Exception {
        StringBuilder log_text = new StringBuilder();
        String line;

        boolean error_flag = false;

        while (!isFinish) {
            synchronized (buffered_reader) {
                if (buffered_reader.ready()) {
                    line = buffered_reader.readLine();
                    log_text.append(line).append("\n");
                    if (line.contains("ERROR")) {
                        error_flag = true;
                        ApplicationManager.is_error_msg = true;
                    }
                }
            }
        }



        return new Pair<>(error_flag, log_text.toString());
    }
}
