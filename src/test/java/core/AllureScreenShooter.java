package core;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.model.Label;
import io.qameta.allure.model.Status;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.reporters.ExitCodeListener;

import static core.ApplicationManager.is_error_msg;
import static test.TestBase.cfg;

/**
 * Selenide screenshots to Allure report.
 * <p>
 * Annotate your test class with <code>@Listeners({AllureScreenShooter.class})</code>.
 * <p>
 * You can configure capture screenshots after success tests by <code>AllureScreenShooter.is_capture_successful_tests = false;</code>.
 * <p>
 * Link {@link com.codeborne.selenide.testng.ScreenShooter}
 */
public class AllureScreenShooter extends ExitCodeListener {
    private static boolean is_capture_successful_tests = false;

    @Attachment(value = "Failure screenshot", type = "image/png")
    private static byte[] getScreenshotBytes() {
        return ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }

    private void analyze_remote_logs() {
        if (cfg.is_analyze_remote_logs()) {
            if (is_error_msg) {
                Allure.getLifecycle().updateTestCase(result -> {
                    result.setName(
                            result.getName() + " [Log ERROR]");
                    result.setStatus(Status.FAILED);
                    result.getLabels().add(new Label().withName("story").withValue("ERRORS in Log"));
                });
            }
        }
    }

    @Override
    public void onTestFailure(ITestResult test_result) {
        super.onTestFailure(test_result);
        getScreenshotBytes();
        analyze_remote_logs();
    }

    @Override
    public void onTestSuccess(ITestResult test_result) {
        super.onTestSuccess(test_result);
        if (is_capture_successful_tests) {
            getScreenshotBytes();
        }
        analyze_remote_logs();
    }

    @Override
    public void onTestSkipped(ITestResult test_result) {
        super.onTestSkipped(test_result);
        analyze_remote_logs();
    }

}
