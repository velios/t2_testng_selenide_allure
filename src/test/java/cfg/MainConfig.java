package cfg;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.LoadType;

import static org.aeonbits.owner.Config.LoadPolicy;
import static org.aeonbits.owner.Config.Sources;

@LoadPolicy(LoadType.MERGE)
@Sources({
        "file:settings.properties",
        "file:auth.properties",
        "file:environment.properties"
})
public interface MainConfig extends Config {
    @Key("internal.user.login")
    String internal_user_login();

    @Key("internal.user.password")
    String internal_user_password();

    @Key("external.user.login")
    String external_user_login();

    @Key("external.user.password")
    String external_user_password();

    @DefaultValue("3")
    String stand_number();

    @Key("stand_${stand_number}.internal.base_url")
    String internal_base_url();

    @Key("stand_${stand_number}.internal.ssh_server.host")
    String internal_ssh_host();
    @Key("stand_${stand_number}.internal.ssh_server.user")
    String internal_ssh_user_login();
    @Key("stand_${stand_number}.internal.ssh_server.auth_key_path")
    String internal_ssh_key();

    @Key("stand_${stand_number}.external.base_url")
    String external_base_url();

    @Key("stand_${stand_number}.external.ssh_server.host")
    String external_ssh_host();
    @Key("stand_${stand_number}.external.ssh_server.user")
    String external_ssh_user_login();
    @Key("stand_${stand_number}.external.ssh_server.auth_key_path")
    String external_ssh_key();

    @Key("browser")
    String browser();

    @Key("browser.hold.open")
    @DefaultValue("false")
    Boolean browser_hold_open();
    
    @Key("browser.headless")
    @DefaultValue("false")
    Boolean browser_headless();
    
    @Key("browser.size")
    @DefaultValue("1920x1080")
    String browser_size();

    @Key("analyze.remote.ssh.logs")
    @DefaultValue("false")
    Boolean is_analyze_remote_logs();
    
    @Key("animation.disabled")
    @DefaultValue("true")
    Boolean is_animation_disabled();
    
    @Key("sample.txt_file.path")
    @DefaultValue(".//src//test//resources//sample_upload_file.txt")
    String sample_txt_file_path();
}