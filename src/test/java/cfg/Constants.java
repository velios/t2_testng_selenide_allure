package cfg;

public interface Constants {
    String STORY_TITLE_ERROR_POPUP_IS_DISPLAYED = "Всплывающее окно с ошибкой";
    String STORY_TITLE_DOWNLOAD_CONTENT = "Скачивание файлов с сайта";
    String STORY_TITLE_CHECK_PASSWORD = "Проверка безопасности паролей";
    String STORY_TITLE_GENERATE_CONTENT = "Генерация новых данных";
    
    String DUMMY_NOT_DATA_IN_TABLE = "Нет никаких данных в таблице";
}
