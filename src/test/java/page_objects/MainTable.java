package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.Ordering;
import core.SortingOptions;
import io.qameta.allure.Step;
import org.testng.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.CollectionCondition.*;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static core.ApplicationManager.blocked_dummy;

public class MainTable {
    private final SelenideElement table = $("#form\\:idMainDataTable");
    private final SelenideElement table_control_panel = table.parent().parent().find("div");
    private final SelenideElement filter_button = table_control_panel.find("span.fa-filter").parent();
    private final SelenideElement sort_button = table_control_panel.find("#form\\:idSortButton");
    private final SelenideElement reset_button = table_control_panel.find("span.fa-rotate-left").parent();
    private final SelenideElement shared_link_button = table_control_panel.find("span.fa-share-alt").parent();
    private final SelenideElement export_to_excel_button = table_control_panel.find(byText("Экспорт в Excel"));
    private final SelenideElement select_table_columns_button = table_control_panel.find("#form\\:toggler");
    private final SelenideElement top_paginator = table.find("#form\\:idMainDataTable_paginator_top");
    private final SelenideElement table_head = table.find("#form\\:idMainDataTable_head");
    private final SelenideElement table_body = table.find("#form\\:idMainDataTable_data");
    private final SelenideElement table_loading_indicator = $(".ajax-loader");
    private ElementsCollection table_rows = table_body.findAll("tr");
    private List<String> table_headers_value_list;
    
    @Step("Производится стандартная проверка фильтрации")
    public void standart_filtering_recipe() {
        String first_cell_value = get_cell(1, "Ид").getText();
        open_filter()
                .set_value("Ид", first_cell_value)
                .submit();
        check_table_should_have_size(1);
        reset_table_state();
    }
    
    @Step("Производится стандартная проверка сортировки")
    public void standart_sorting_recipe() {
        open_sort()
                .set_value("Ид", SortingOptions.ASCENDING)
                .submit();
        check_table_column_sorted("Ид", SortingOptions.ASCENDING);
        reset_table_state();
    }
    
    @Step("Сброс состояния таблицы кнопкой reset")
    public void reset_table_state() {
        wait_loading();
        reset_button.click();
        
        check_table_have_rows();
        table_rows.shouldHave(sizeGreaterThan(0));
    }
    
    @Step("Выбрать ячейку в строке номер {row_number} в колонке {column_heading}")
    public SelenideElement get_cell(Integer row_number, String column_heading) {
        wait_loading();
        init_table();
        check_table_have_rows();
        int row_index = row_number - 1;
        int col_index = table_headers_value_list.indexOf(column_heading.toLowerCase());
        SelenideElement cell_element = table_rows.get(row_index).findAll("td").get(col_index);
        cell_element.scrollIntoView(false);
        if (cell_element.find("a").exists()) {
            return cell_element.find("a");
        } else if (cell_element.find("*").exists()) {
            return cell_element.find("*");
        } else {
            return cell_element;
        }
    }
    
    @Step("Выбрать строку номер {row_number}")
    public SelenideElement get_row(Integer row_number) {
        wait_loading();
        init_table();
        check_table_have_rows();
        int row_index = row_number - 1;
        return table_rows.get(row_index);
    }
    
    @Step("[Проверка] Заголовки таблицы соотвтствуют: {table_headers_list}")
    public MainTable check_headers_text(String... table_headers_list) {
        wait_loading();
        table_head
                .findAll("span")
                .shouldHave(exactTexts(table_headers_list));
        return this;
    }
    
    @Step("[Проверка] Таблица состоит из {rows_count} строк(и)")
    public MainTable check_table_should_have_size(Integer rows_count) {
        wait_loading();
        check_table_have_rows();
        table_rows.shouldHave(size(rows_count));
        return this;
    }
    
    @Step("[Проверка] Таблица отсортирована по колонке {column_heading} по порядку {sort_order}")
    public MainTable check_table_column_sorted(String column_heading, SortingOptions sort_order) {
        wait_loading();
        init_table();
        ArrayList<Integer> sorted_row_list = new ArrayList<>();
        int col_index = table_headers_value_list.indexOf(column_heading.toLowerCase());
        
        for (SelenideElement item : table_rows) {
            int integerValue;
            SelenideElement element = item.findAll("td").get(col_index);
            if (element.find("a").exists()) {
                integerValue = Integer.parseInt(element.find("a").getText());
            } else {
                integerValue = Integer.parseInt(element.getText());
            }
            sorted_row_list.add(integerValue);
        }
        
        if (sort_order == SortingOptions.DESCENDING) {
            Assert.assertTrue(Ordering.natural().reverse().isOrdered(sorted_row_list));
        } else {
            Assert.assertTrue(Ordering.natural().isOrdered(sorted_row_list));
        }
        return this;
    }
    
    public FilterForm open_filter() {
        wait_loading();
        filter_button.click();
        return new FilterForm();
    }
    
    public SortingForm open_sort() {
        wait_loading();
        sort_button.click();
        return new SortingForm();
    }
    
    @Step("[Проверка] В таблице есть хотя бы одна строка")
    private void check_table_have_rows() {
        wait_loading();
        if (table_rows.first().find("td").has(Condition.text("Записей не найдено"))) {
            blocked_dummy("В таблице нет записей");
        }
    }
    
    private void init_table() {
        table_headers_value_list = table_head.findAll("th").stream()
                                           .map(element -> element.getText().trim().toLowerCase())
                                           .collect(Collectors.toList());
    }
    
    private void wait_loading() {
        // TODO: 018 18.08.18 Сделать через proxy, который будет вызываться перед каждым методом MainTable
        if (table_loading_indicator.exists()) {
            table_loading_indicator.waitUntil(not(visible), 15000);
        }
        //Большой индикатор ожидания у некоторых таблиц
        if ($(".ui-blockui-content").exists()) {
            $(".ui-blockui-content").waitUntil(not(visible), 30000);
        }
    }
    
    @Step("Скачивание по кнопке таблицы 'Экспорт в Excel'")
    public FileWrapper export_to_excel_download() {
        File downloaded_file = null;
        try {
            downloaded_file = export_to_excel_button.download();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return new FileWrapper(downloaded_file);
    }
    
}
