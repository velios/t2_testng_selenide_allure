package page_objects;

import io.qameta.allure.Step;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.testng.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileWrapper {
    private File file;
    
    FileWrapper(File file) {
        this.file = file;
    }
    
    @Step("[Проверка] Расширение файла равно: {expected_extension}")
    public FileWrapper check_file_extension(String expected_extension) {
        String extension = FilenameUtils.getExtension(file.getName());
        Assert.assertEquals(extension, expected_extension);
        return this;
    }
    
    @Step("[Проверка] Имя файла без расширения равно: {expected_basename}")
    public FileWrapper check_file_base_name(String expected_basename) {
        String base_name = FilenameUtils.getBaseName(file.getName());
        Assert.assertEquals(base_name, expected_basename);
        return this;
    }
    
    @Step("[Проверка] Полное имя файла равен: {expected_filename}")
    public FileWrapper check_file_name(String expected_filename) {
        String file_name = file.getName();
        Assert.assertEquals(file_name, expected_filename);
        return this;
    }
    
    @Step("[Проверка] Полное имя файла содержит: {expected_basename_start}")
    public FileWrapper check_base_name_contains(String expected_basename_start) {
        String file_name = file.getName();
        Assert.assertTrue(file_name.contains(expected_basename_start));
        return this;
    }
    
    @Step("[Проверка] Md5-хэш файла равен: {expected_md5_hash}")
    public FileWrapper check_md5_hash(String expected_md5_hash) {
        String md5_hash = null;
        try {
            md5_hash = DigestUtils.md5Hex(IOUtils.toByteArray(new FileInputStream(file)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(md5_hash, expected_md5_hash);
        return this;
    }
    
    
}
