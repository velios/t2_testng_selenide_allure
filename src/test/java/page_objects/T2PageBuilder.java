package page_objects;

public class T2PageBuilder {
    private String base_url = "";
    private String start_page_path = "";
    private String start_page_title = "";
    
    
    public T2PageBuilder with_base_url(String base_url) {
        this.base_url = base_url;
        return this;
    }
    
    public T2PageBuilder with_start_page_path(String start_page_path) {
        this.start_page_path = start_page_path;
        return this;
    }
    
    public T2PageBuilder with_start_page_title(String start_page_title) {
        this.start_page_title = start_page_title;
        return this;
    }
    
    public T2Page build() {
        return new T2Page(
                base_url,
                start_page_path,
                start_page_title
        );
    }
}
