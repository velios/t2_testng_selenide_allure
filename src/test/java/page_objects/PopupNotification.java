package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.$;

public class PopupNotification {
    private final SelenideElement popup_body = $("#form\\:growl_container");
    private SelenideElement current_popup_container = popup_body.find("> div");
    
    @Step("[Проверка] Сообщение pop-up содержит {text}")
    public PopupNotification with_message_contains(String text) {
        //Case insensitive
        SelenideElement popup_message_element = popup_body
                                                        .find(withText(text))
                                                        .shouldBe(Condition.visible);
        current_popup_container = popup_message_element
                                          .parent().parent().parent();
        return this;
    }
    
    @Step("[Проверка] Заголовок pop-up содержит {text}")
    public PopupNotification check_title_contains(String text) {
        //Case insensitive
        current_popup_container
                .find("div.ui-growl-message")
                .find("span.ui-growl-title")
                .shouldHave(Condition.text(text));
        return this;
    }
    
    @Step("[Проверка] pop-up с ошибкой?")
    public PopupNotification check_is_error() {
        // TODO: 012 12.08.18 Более информативные сообщения об ошибке. Что не найдено именно окно ошибки
        current_popup_container
                .shouldHave(Condition.cssClass("ui-growl-error"));
        return this;
    }
    
    @Step("[Проверка] Информационный pop-up?")
    public PopupNotification check_is_info() {
        // TODO: 012 12.08.18 Более информативные сообщения об ошибке. Что не найдено именно окно информации
        current_popup_container
                .shouldHave(Condition.cssClass("ui-growl-info"));
        return this;
    }
}
