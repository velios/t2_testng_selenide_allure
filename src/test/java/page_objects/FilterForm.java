package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class FilterForm {
    private final SelenideElement filter_dialog = $("#form\\:idFilterDialog");
    private final ElementsCollection content_table = filter_dialog
                                                             .find("div.ui-dialog-content")
                                                             .find("tbody")
                                                             .findAll("tr");
    private final SelenideElement submit_button = filter_dialog.find("button[type='submit']");
    private List<String> table_headers_value_list;
    
    FilterForm() {
        filter_dialog.shouldBe(Condition.visible);
        sleep(500);
        table_headers_value_list = filter_dialog
                                           .find("div.ui-dialog-content")
                                           .find("table")
                                           .find("tbody")
                                           .findAll("tr > td:first-child")
                                           .stream().map(element -> element.getText().trim().replaceFirst(":$", "").toLowerCase())
                                           .collect(Collectors.toList());
    }
    
    @Step("Заполнить в фильтре поле {name} значением value")
    public FilterForm set_value(String name, String value) {
        int row_index = table_headers_value_list.indexOf(name.trim().replaceFirst(":$", "").toLowerCase());
        ElementsCollection input_row = content_table.get(row_index).findAll("> td");
        input_row.get(1).scrollIntoView(false).click();
        input_row.get(2).find("input").shouldBe(Condition.enabled).setValue(value);
        return this;
    }
    
    @Step("Применить новые значения фильтра")
    public void submit() {
        submit_button.scrollIntoView(false).click();
    }
    
    
}
