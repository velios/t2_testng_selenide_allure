package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import core.ApplicationManager;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.qameta.allure.model.Link;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;
import static test.TestBase.cfg;

public class T2Page {
    
    private final SelenideElement h1_header = $("h1");
    private String base_url;
    private String start_page_path;
    private String start_page_title;
    
    T2Page(String base_url, String start_page_path, String start_page_title) {
        this.base_url = base_url;
        this.start_page_path = start_page_path;
        this.start_page_title = start_page_title;
    }
    
    @Step("Открытие страницы {path}")
    public T2Page open(String path) {
        String url = base_url + path;
        Selenide.open(url);
        
        //Добавить ссылку на открытую страницу в описание теста в Allure
        Allure.getLifecycle().updateTestCase(result -> {
            List<Link> links = result.getLinks();
            if (links.stream().noneMatch(link -> link.getName().equals(title()))) {
                links.add(new Link().withName(title()).withUrl(url));
            }
        });
        
        // Отключить анимацию
        if (cfg.is_animation_disabled()) {
            ApplicationManager.disable_animation();
        }
        
        return this;
    }
    
    @Step("Открытие стартовой страницы")
    public T2Page open_start_page() {
        open(start_page_path).check_title(start_page_title);
        return this;
    }
    
    @Step("[Проверка] Заголовок странциы равен: {title}")
    public T2Page check_title(String title) {
        $("title").shouldHave(attribute("text", title));
        return this;
    }
    
    @Step("Скачивание элемента {element}")
    public FileWrapper download_element(SelenideElement element) {
        File downloaded_file = null;
        try {
            downloaded_file = element.download();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return new FileWrapper(downloaded_file);
    }
    
    @Step("[Проверка] Заголовок H1 содержит: {text}")
    public void check_h1_header_contains(String text) {
        h1_header.shouldHave(Condition.text(text));
    }
    
    @Step("[Проверка] Заголовок H1 содержит один из вариантов: {text_variants}")
    public void check_h1_header_contains(String... text_variants) {
        // FIXME: 013 13.08.18 Убрать принудительную задержку. Научиться определять что после клика страница полностью загрузилась
        Selenide.sleep(1000);
    
        String h1_header_text = h1_header.getText().trim();
        if (Arrays.stream(text_variants).noneMatch(h1_header_text::contains)) {
            throw new AssertionError(
                    String.format(
                            "Заголовок H1 '%s' не содержит не один из предложенных вариантов: %s",
                            h1_header_text,
                            Arrays.toString(text_variants)
                    ));
        }
    }
}
