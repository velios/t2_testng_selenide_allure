package page_objects;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.NotFoundException;

import java.io.File;
import java.io.FileNotFoundException;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class NavigationMenu {
    
    private final SelenideElement menu = $(".layout-menu-container");
    
    private SelenideElement fetch_item(String item_title) {
        SelenideElement menu_item = menu.find(byText(item_title)).parent().parent();
        SelenideElement menu_item_section = menu_item.parent().parent();
        SelenideElement menu_section;
        while (!menu_item.isDisplayed()) {
            menu_section = menu_item_section;
            while (!menu_section.isDisplayed()) {
                SelenideElement previous_menu_section = menu_section;
                menu_section = fetch_superior_section(menu_section);
                if (menu_section.equals(previous_menu_section)) {
                    break;
                }
            }
            menu_section.scrollIntoView(true).shouldBe(visible).click();
            menu_section.findAll("> ul > li").last().shouldBe(visible); //проверка полного открытия меню
        }
        menu_item.scrollIntoView(true).shouldBe(visible);
        return menu_item;
    }
    
    @Step("Скачивание из пункта меню: '{item_title}'")
    public FileWrapper download_item(String item_title) {
        SelenideElement menu_item = fetch_item(item_title);
        File downloaded_file = null;
        try {
            downloaded_file = menu_item.download();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return new FileWrapper(downloaded_file);
    }
    
    private SelenideElement fetch_superior_section(SelenideElement menu_section) {
        SelenideElement under_menu_section = menu_section.parent().parent();
        if (!under_menu_section.exists()) {
            throw new NotFoundException("Некорректный поиск по меню");
        }
        if (under_menu_section.has(not(
                attribute("role", "menuitem")))) {
            //Если корневой раздел меню
            return menu_section;
        }
        return under_menu_section;
    }
    
    
}
