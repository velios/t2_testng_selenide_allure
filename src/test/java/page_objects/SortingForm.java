package page_objects;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.SortingOptions;
import io.qameta.allure.Step;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class SortingForm {
    private final SelenideElement sorting_dialog = $("#form\\:idSortingDialog");
    private final ElementsCollection content_table = sorting_dialog
                                                             .find("div.ui-dialog-content")
                                                             .find("tbody")
                                                             .findAll("tr");
    private final SelenideElement submit_button = sorting_dialog.find("button[type='submit']");
    private List<String> table_headers_value_list;
    
    SortingForm() {
        sorting_dialog.shouldBe(Condition.visible);
        // FIXME: 023 23.08.18 Убрать принудительную задержку
        sleep(1000);
        table_headers_value_list = sorting_dialog
                                           .find("div.ui-dialog-content")
                                           .find("tbody")
                                           .findAll("tr > td:first-child")
                                           .stream().map(element -> element.getText().trim().replaceFirst(":$", "").toLowerCase())
                                           .collect(Collectors.toList());
    }
    @Step("Заполнить в фильтре поле {name} значением sort_order")
    public SortingForm set_value(String name, SortingOptions sort_order) {
        int row_index = table_headers_value_list.indexOf(name.toLowerCase());
        ElementsCollection input_row = content_table.get(row_index).findAll("> td");
        SelenideElement order_button = input_row.get(1).find("div.ui-buttonset").findAll("> div").get(sort_order.ordinal());
        order_button.click();
        return this;
    }
    
    @Step("Применить новые значения сортировки")
    public void submit() {
        submit_button.scrollIntoView(false).click();
    }
    
}
