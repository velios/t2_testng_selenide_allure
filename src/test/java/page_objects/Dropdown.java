package page_objects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Dropdown {
    private SelenideElement dropdown_root_element;
    private SelenideElement dropdown_options;
    
    public Dropdown(SelenideElement dropdown_root_element) {
        this.dropdown_root_element = dropdown_root_element;
        this.dropdown_options =
                $(By.id(dropdown_root_element.attr("id") + "_panel"));
    }
    
    public Dropdown choose_option_by_value(String value) {
        dropdown_root_element.click();
        dropdown_options
                .find("ul")
                .findAll("li")
                .stream()
                .filter(element -> element.attr("data-label").equals(value))
                .findFirst()
                .orElse(null);
        
        if (dropdown_options != null) {
            dropdown_options
                    .scrollIntoView(true)
                    .click();
        }
        
        return this;
    }
    
    public Dropdown choose_option_by_position(Integer index) {
        dropdown_root_element.click();
        dropdown_options
                .find("ul")
                .findAll("li")
                .get(index)
                .scrollIntoView(true)
                .click();
        return this;
    }
    
    public Dropdown choose_last_option() {
        dropdown_root_element.click();
        dropdown_options
                .find("ul")
                .findAll("li")
                .last()
                .scrollIntoView(true)
                .click();
        return this;
    }
    
}
