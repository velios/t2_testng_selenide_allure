package test;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class OrderReportsSteps extends EngineerTestsBase {
    
    @Step("Система открывает страницу \"Запрос выписки\"")
    void T2_57_step_1() {
        engineer_page
                .open("/print-statement.xhtml")
                .check_title("Запрос выписки");
    }
    
    @Step("Пользователь вводит дату в “Начальная дата” и “Конечная дата”. Нажимает \"Сформировать\"")
    void T2_57_step_2() {
        app.blocked_dummy("При выборе любого работадателя пишет - Для выбранного работодателя не поддерживается интеграция с 1С");
    }
    
    @Step("Пользователь открывает страницу \"Запрос информации о назначенных задачах\"")
    void T2_58_step_1() {
        engineer_page
                .open("/print-to-do-list.xhtml")
                .check_title("Запрос информации о назначенных задачах");
    }
    
    @Step("Пользователь нажимает \"Сформировать\"")
    void T2_58_step_2() {
        app.blocked_dummy("Кнопка Сформировать не работает");
    }
    
    @Step("Пользователь открывает страницу \"Печать выполненных работ\"")
    void T2_59_step_1() {
        engineer_page
                .open("/print-permanent-acts-report.xhtml")
                .check_title("Печать отчетов по актам");
    }
    
    @Step("Пользователь вводит период выполнения, период активирования, инженер, филиал, статус записи, порядок сортировки. Нажимает \"Сформировать\"")
    void T2_59_step_2() {
        SelenideElement completion_period_checkbox = $("#form\\:useCompletionPeriod").find(".ui-chkbox-box");
        SelenideElement completion_period_start_date = $("form\\:j_idt56");
        SelenideElement completion_period_end_date = $("form\\:j_idt57");
        
        SelenideElement billing_period_checkbox = $("#form\\:useBillingPeriod").find(".ui-chkbox-box");
        SelenideElement billing_period_start_date = $("form\\:billingDateFrom");
        SelenideElement billing_period_end_date = $("form\\:billingDateTo");

//        SelenideElement submit_button = $(byText("Сформировать"));
//        app.getDownloadsHelper().download_file_and_compare_extention_and_filename(
//                submit_button,
//                "xlsx",
//                "acts_report"
//        );
        
        // TODO: 006 06.08.18 Можно реализовывать, остался один шаг. Доделать полноценное заполнение формы
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь оставляет поля пустыми. Нажимает \"Сформировать\"")
    void T2_60_step_1() {
        engineer_page
                .open("/print-permanent-acts-report.xhtml")
                .check_title("Печать отчетов по актам");
        
        $(byText("Сформировать")).click();
        
        popup
                .with_message_contains("Необходимо выбрать хотя бы один период")
                .check_is_error();
    }
}
