package test;

import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "1 Настройки SOAP веб-сервисов")
public class SetupSOAPWebServicesTests extends SetupSOAPWebServicesSteps {
    
    @Test(description = "T2-241: Дескрипторы SOAP веб-сервисов")
    @TmsLink("1140")
    public void T2_241() {
        T2_241_step_1();
        T2_241_step_2();
        T2_241_step_3();
        T2_241_step_4();
        T2_241_step_5();
    }
    
    @Test(description = "T2-242: Параметры интеграции с 1С")
    @TmsLink("1141")
    public void T2_242() {
        T2_242_step_1();
        T2_242_step_2();
    }

}
