package test;

import cfg.Constants;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "3 Скачать")
public class DownloadTests extends DownloadSteps {
    // FIXME: 010 10.08.18 Некорректная работа NavigationMenu.fetch_item при выполнении полного Suite. Залипает на частично открытом меню

    @Test(description = "T2-63:Дистрибутив мобильного приложения")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1181")
    public void T2_63() {
        T2_63_step_1();
    }

    @Test(description = "T2-67:Иструкция по работе с мобильным приложением")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1182")
    public void T2_67() {
        T2_67_step_1();
    }

    @Test(description = "T2-62:Дистрибутив Google play services")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1183")
    public void T2_62() {
        T2_62_step_1();
    }

    @Test(description = "T2-65:Инструкция для проверки и обновления Google play services")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1184")
    public void T2_65() {
        T2_65_step_1();
    }

    @Test(description = "T2-71:Требования к мобильному устройству")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1185")
    public void T2_71() {
        T2_71_step_1();
    }

    @Test(description = "T2-64:Изменения в мобильном приложении")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1186")
    public void T2_64() {
        T2_64_step_1();
    }

    @Test(description = "T2-70:Правила тарификации на сервисной площадке")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1187")
    public void T2_70() {
        T2_70_step_1();
    }

    @Test(description = "T2-68:Описание базовых подклассов ТМЦ включенных в тариф")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1188")
    public void T2_68() {
        T2_68_step_1();
    }

    @Test(description = "T2-69:Позиции ТМЦ включенные в тариф")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1189")
    public void T2_69() {
        T2_69_step_1();
    }

    @Test(description = "T2-72:Шаблоны акта выполненных работ")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1190")
    public void T2_72() {
        T2_72_step_1();
    }

    @Test(description = "T2-73:Штрих-М (ЗАО)_Прошивка для НОВОЙ платформы ККТ")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1191")
    public void T2_73() {
        T2_73_step_1();
    }

    @Test(description = "T2-74:Штрих-М Прошивка для НОВОЙ платформы ККТ")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1192")
    public void T2_74() {
        T2_74_step_1();
    }

    @Test(description = "T2-61:PAYKIOSK_Прошивки_устранение проблемы")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1193")
    public void T2_61() {
        T2_61_step_1();
    }

    @Test(description = "T2-252:FIT_NEW_Прошивка Ритейл устранение сбоя 20.12")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1194")
    public void T2_252() {
        T2_252_step_1();
    }

    @Test(description = "T2-66:Инструкция_Обновление ПО ККМ новой платформы Штрих через TeraTerm общая")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1197")
    public void T2_66() {
        T2_66_step_1();
    }

    @Test(description = "LVE_1: Инструкции по контрактации")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    public void LVE_1() {
        LVE1_step_1();
    }

}
