package test;

import cfg.MainConfig;
import core.AllureScreenShooter;
import core.ApplicationManager;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.*;
import page_objects.*;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@Listeners({AllureScreenShooter.class})
public class TestBase {
    
    public static final MainConfig cfg = ConfigFactory.create(MainConfig.class);
    ApplicationManager app = new ApplicationManager();
    
    T2Page system_user_page =
            new T2PageBuilder()
                    .with_base_url(cfg.internal_base_url() + "/rigel/internal")
                    .with_start_page_path("/internal-dashboard.xhtml")
                    .with_start_page_title("Рабочий стол для сотрудников Maykor")
                    .build();
    
    T2Page engineer_page =
            new T2PageBuilder()
                    .with_base_url(cfg.external_base_url() + "/rigel/external")
                    .with_start_page_path("/freelancer-dashboard.xhtml")
                    .with_start_page_title("Рабочий стол инженера")
                    .build();
    
    NavigationMenu menu = new NavigationMenu();
    PopupNotification popup = new PopupNotification();
    MainTable table = new MainTable();
    
    @BeforeSuite(description = "Инициализация Suite", alwaysRun = true)
    public void suiteSetUp() {
        app.suite_init();
    }
    
    @BeforeMethod(description = "Инициализация Теста", alwaysRun = true)
    public void methodSetUp() {
        app.method_init();
    }
    
    @AfterMethod(description = "Закрытие Теста", alwaysRun = true)
    public void methodTearDown() {
        try {
            app.method_stop();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    @AfterSuite(description = "Закрытие Suite", alwaysRun = true)
    public void suiteTearDown() {
        try {
            app.suite_stop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
