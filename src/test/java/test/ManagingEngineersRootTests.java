package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;


@Test(testName = "1 Управление инженерами | /")
public class ManagingEngineersRootTests extends ManagingEngineersRootSteps {
    
    @Test(description = "T2-200: Запросы на первичную регистрацию")
    @TmsLink("1062")
    public void T2_200() {
        T200_step_1();
        T200_step_2();
        T200_step_3();
        T200_step_4();
        T200_step_5();
        T200_step_6();
        T200_step_7();
        T200_step_8();
        T200_step_9();
    }
    
    @Test(description = "T2-149: Черный список")
    @TmsLink("1063")
    public void T2_149() {
        T149_step_1();
        T149_step_2();
    }
    
    @Test(description = "T2-145:Запросы инженеров")
    @TmsLink("1064")
    public void T2_145() {
        T145_step_1();
        T145_step_2();
        T145_step_3();
        T145_step_4();
    }

    @Test(description = "T2-201:Печать выполненных работ")
    @Issue("D-609")
    @TmsLink("1065")
    public void T2_201() {
        T201_step_1();
        T201_step_2();
        T201_step_3();
        T201_step_4();
        T201_step_5();
        T201_step_6();
        T201_step_7();
        T201_step_8();
        T201_step_9();
        T201_step_10();
        T201_step_11();
    }
    
    @Test(description = "T2-146:Инженеры")
    @TmsLink("1066")
    public void T2_146() {
        T146_step_1();
        T146_step_2();
        T146_step_3();
    }

}
