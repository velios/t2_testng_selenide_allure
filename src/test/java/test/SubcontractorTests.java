package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "211 НСИ | Общие данные | Хоз.субъекты")
public class SubcontractorTests extends SubcontractorSteps {
    
    @Test(description = "T2-140:Все хоз. субъекты")
    @TmsLink("1081")
    public void T2_140() {
        T2_140_step_1();
        T2_140_step_2();
        T2_140_step_3();
        T2_140_step_4();
        T2_140_step_5();
        T2_140_step_6();
        T2_140_step_7();
        T2_140_step_8();
        T2_140_step_9();
        T2_140_step_10();
        T2_140_step_11();
        T2_140_step_12();
        T2_140_step_13();
    }

    @Test(description = "T2-204: Субподрядчики в связке с объектами Remedy")
    @Issue("D-617")
    @TmsLink("1082")
    public void T2_204() {
        T2_204_step_1();
        T2_204_step_2();
        T2_204_step_3();
    }
    
    @Test(description = "T2-253:Субподрядные договора")
    @TmsLink("1083")
    public void T2_253() {
        T2_253_step_1();
        T2_253_step_2();
        T2_253_step_3();
        T2_253_step_4();
        T2_253_step_5();
        T2_253_step_6();
        T2_253_step_7();
        T2_253_step_8();
    }
    
    @Test(description = "T2-254: Проверка маршрутизации на субподрядчика")
    @TmsLink("1084")
    public void T2_254() {
        T2_254_step_1();
        T2_254_step_2();
    }

}
