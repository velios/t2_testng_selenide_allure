package test;

import cfg.Constants;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "2221 НСИ | Обработка заявок | Ценообразование | Ценообрзование для субподрядчика")
public class SubcontractorPricingTests extends SubcontractorPricingSteps {
    
    
    @Test(description = "T2-121:Приоритет правил ценообразования для субподрядчиков")
    @TmsLink("1092")
    @TmsLink("1092")
    public void T2_121() {
        T2_121_step_1();
        T2_121_step_2();
        T2_121_step_3();
        T2_121_step_4();
        T2_121_step_5();
    }

    @Test(description = "T2-120: Приоритет правил ценообразования для субподрядчика - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1093")
    public void T2_120() {
        T2_120_step_1();
    }
    
    @Test(description = "T2-115:Правила ценообразования для субподрядчиков")
    @TmsLink("1094")
    public void T2_115() {
        T2_115_step_1();
        T2_115_step_2();
        T2_115_step_3();
        T2_115_step_4();
        T2_115_step_5();
        T2_115_step_6();
        T2_115_step_7();
    }

    @Test(description = "T2-116:Правила ценообразования для субподрядчиков - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1095")
    public void T2_116() {
        T2_116_step_1();
    }
    
    @Test(description = "T2-122:Проверка расчета цены для субподрядчика")
    @TmsLink("1096")
    public void T2_122() {
        T2_122_step_1();
        T2_122_step_2();
    }

    @Test(description = "T2-123:Проверка расчета цены для субподрядчика - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1097")
    public void T2_123() {
        T2_123_step_1();
    }

    @Test(description = "T2-113:Группы ценообразования")
    @Issue("D-616")
    @TmsLink("1098")
    public void T2_113() {
        T2_113_step_1();
        T2_113_step_2();
        T2_113_step_3();
        T2_113_step_4();
        T2_113_step_5();
    }

    @Test(description = "T2-114:Группы ценообразования - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1099")
    public void T2_114() {
        T2_114_step_1();
    }

}
