package test;

import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "/")
public class AdministrationRootTests extends AdministrationRootSteps {
    
    @Test(description = "T2-239:Системные параметры")
    @TmsLink("1134")
    public void T2_239() {
        T2_239_step_1();
        T2_239_step_2();
    }
    
    @Test(description = "T2-240:Системные уведомления")
    @TmsLink("1135")
    public void T2_240() {
        T2_240_step_1();
        T2_240_step_2();
        T2_240_step_3();
        T2_240_step_4();
    }
    
    @Test(description = "T2-195:Системные СМС")
    @TmsLink("1136")
    public void T2_195() {
        T2_195_step_1();
        T2_195_step_2();
        T2_195_step_3();
        T2_195_step_4();
    }
    
    @Test(description = "T2-12:Журнал аудита")
    @TmsLink("1137")
    public void T2_12() {
        T2_12_step_1();
        T2_12_step_2();
    }
    
    @Test(description = "T2-14:Периодические задания")
    @TmsLink("1138")
    public void T2_14() {
        T2_14_step_1();
        T2_14_step_2();
    }
    
    @Test(description = "T2-244:Доверенные отправители АВР")
    @TmsLink("1139")
    public void T2_244() {
        T2_244_step_1();
        T2_244_step_2();
        T2_244_step_3();
        T2_244_step_4();
        T2_244_step_5();
    }
}
