package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "24 НСИ | Remedy-справочники")
public class RemedyCatalogsTests extends RemedyCatalogsSteps {
    
    @Test(description = "T2-233:Группы поддержки Remedy")
    @TmsLink("1125")
    public void T2_233() {
        T2_233_step_1();
        T2_233_step_2();
    }

    @Test(description = "T2-234:Справочник пользователей")
    @Issue("D-613")
    @Issue("D-615")
    @TmsLink("1126")
    public void T2_234() {
        T2_234_step_1();
        T2_234_step_2();
    }

}
