package test;

import cfg.Constants;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "/")
public class EngineerSectionRootTests extends EngineerSectionRootSteps {
    
    @Test(description = "T2-245:Рабочий стол")
    @TmsLink("1153")
    public void T2_245() {
        T2_245_step_1();
    }
    
    @Test(description = "T2-54:Профиль инженера")
    @TmsLink("1154")
    public void T2_54() {
        T2_54_step_1();
        T2_54_step_2();
    }
    
    @Test(description = "T2-42:Запросы на изминение профиля")
    @Issue("D-607")
    @TmsLink("1155")
    public void T2_42() {
        T2_42_step_1();
        T2_42_step_2();
        T2_42_step_3();
    }
    
    @Test(description = "T2-40:Заказанные отчеты")
    @TmsLink("1156")
    public void T2_40() {
        T2_40_step_1();
        T2_40_step_2();
    }
    
    @Test(description = "T2-208:Вопросы и ответы")
    @Story(Constants.STORY_TITLE_DOWNLOAD_CONTENT)
    @TmsLink("1157")
    public void T2_208() {
        T2_208_step_1();
    }
    
    @Test(description = "T2-43:Изменить пароль")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1158")
    public void T2_43() {
        T2_43_step_1();
        T2_43_step_2();
    }
    
    @Test(description = "T2-44:Изменить пароль - Совпадающий пароль")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1159")
    public void T2_44() {
        T2_44_step_1();
    }
    
    @Test(description = "T2-45:Изменить пароль - Некорректный старый пароль")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1160")
    public void T2_45() {
        T2_45_step_1();
    }
    
    @Test(description = "T2-46:Изменить пароль - Несовпадающие новый пароль и подтверждение")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1161")
    public void T2_46() {
        T2_46_step_1();
    }
    
    @Test(description = "T2-47:Изменить пароль - Пустые поля")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1162")
    public void T2_47() {
        T2_47_step_1();
    }
    
    @Test(description = "T2-49:Изменить пароль - Только нижний регистр букв")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1163")
    public void T2_49() {
        T2_49_step_1();
    }
    
    @Test(description = "T2-48:Изменить пароль - Пароль менее 6 символов")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1164")
    public void T2_48() {
        T2_48_step_1();
    }
    
    @Test(description = "T2-50:Изменить пароль - Только верхний регистр букв")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1165")
    public void T2_50() {
        T2_50_step_1();
    }
    
    @Test(description = "T2-51:Изменить пароль - Только цифры")
    @Story(Constants.STORY_TITLE_CHECK_PASSWORD)
    @TmsLink("1166")
    public void T2_51() {
        T2_51_step_1();
    }
    
    @Test(description = "T2-35:Акты выполненных работ")
    @TmsLink("1167")
    public void T2_35() {
        T2_35_step_1();
        T2_35_step_2();
        T2_35_step_3();
    }
    
    @Test(description = "T2-36:Акты выполненных работ - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1168")
    public void T2_36() {
        T2_36_step_1();
    }
    
    @Test(description = "T2-53:Мои сообщения")
    @TmsLink("1169")
    public void T2_53() {
        T2_53_step_1();
    }
    
    @Test(description = "T2-56:Розыгрыш новой заявки")
    @TmsLink("1170")
    public void T2_56() {
        T2_56_step_1();
        T2_56_step_2();
        T2_56_step_3();
    }
    
    @Test(description = "T2-169:Запрос на регистрацию инженера")
    @TmsLink("1171")
    public void T2_169() {
        T2_169_step_1();
        T2_169_step_2();
        T2_169_step_3();
        T2_169_step_4();
        T2_169_step_5();
        T2_169_step_6();
        T2_169_step_7();
        T2_169_step_8();
    }
    
    @Test(description = "T2-260:Заказ-наряды")
    public void T2_280() {
        // TODO: 023 23.08.18 Добавить ссылку на TestLink
        T2_280_step_1();
        T2_280_step_2();
        T2_280_step_3();
    }
}
