package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "1 Контрактация")
public class ContractingTests extends ContractingSteps {
    
    @Test(description = "T2-170:Запрос на установление трудовых отношений с работодателем")
    @TmsLink("1172")
    public void T2_170() {
        T2_170_step_1();
        T2_170_step_2();
        T2_170_step_3();
        T2_170_step_4();
        T2_170_step_5();
        T2_170_step_6();
        T2_170_step_7();
        T2_170_step_8();
        T2_170_step_9();
        T2_170_step_10();
        T2_170_step_11();
        T2_170_step_12();
    }
    
    @Test(description = "T2-171:Запрос на установление трудовых отношений с работодателем (повторное)")
    @TmsLink("1173")
    public void T2_171() {
        T2_171_step_1();
        T2_171_step_2();
        T2_171_step_3();
    }

    @Test(description = "T2-165:Предложения о работе")
    @Issue("D-606")
    @TmsLink("1174")
    public void T2_165() {
        T2_165_step_1();
        T2_165_step_2();
        T2_165_step_3();
        T2_165_step_4();
        T2_165_step_5();
        T2_165_step_6();
        T2_165_step_7();
    }
    
    @Test(description = "T2-166:Работодатели")
    @TmsLink("1175")
    public void T2_166() {
        T2_166_step_1();
    }
    
    @Test(description = "T2-167:Запросы на прекращение трудовых отношений")
    @TmsLink("1176")
    public void T2_167() {
        T2_167_step_1();
    }
}
