package test;

import cfg.Constants;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "11 Управление инженерами | Контрактация с инженерами")
public class EngineersContractingTests extends EngineersContractingSteps {

    @Test(description = "T2-161:Предложения о работе")
    @Issue("D-606")
    @TmsLink("1067")
    public void T2_161() {
        T2_161_step_1();
        T2_161_step_2();
        T2_161_step_3();
        T2_161_step_4();
        T2_161_step_5();
        T2_161_step_6();
        T2_161_step_7();
    }

    @Test(description = "T2-162:Предложения о работе - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1068")
    public void T2_162() {
        T2_162_step_1();
    }
    
    @Test(description = "T2-172: Запрос на установление трудовых отношений с работодателем")
    @TmsLink("1069")
    public void T2_172() {
        T2_172_step_1();
        T2_172_step_2();
        T2_172_step_3();
        T2_172_step_4();
        T2_172_step_5();
        T2_172_step_6();
        T2_172_step_7();
        T2_172_step_8();
    }

    @Test(description = "T2-163:Запросы на прекращение трудовых отношений")
    @Issue("D-608")
    @TmsLink("1070")
    public void T2_163() {
        T2_163_step_1();
        T2_163_step_2();
        T2_163_step_3();
        T2_163_step_4();
        T2_163_step_5();
    }

}
