package test;

import io.qameta.allure.Step;

public class DownloadSteps extends EngineerTestsBase {
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_63_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Дистрибутив мобильного приложения")
                .check_md5_hash("03f58c3f2e7df85b5c826f1f6620285d");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_67_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Иструкция по работе с мобильным приложением")
                .check_md5_hash("4abb502d9125cb2109693c2422e8e2f0");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_62_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Дистрибутив Google play services")
                .check_md5_hash("ace84f0b16e5109315b8da2492714799");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_65_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Инструкция для проверки и обновления Google play services")
                .check_md5_hash("23602c3f5b2ffec387d0855a54c9edd5");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_71_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Требования к мобильному устройству")
                .check_md5_hash("d0a7301fe8663d204c223ad32d00cfc2");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_64_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Изменения в мобильном приложении")
                .check_md5_hash("39d9996b0f2ef0447b99911cbd802060");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_70_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Правила тарификации на сервисной площадке")
                .check_md5_hash("8c96525f62d1b8a65538ac28cfd6d3e7");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_68_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Описание базовых подклассов ТМЦ включенных в тариф")
                .check_md5_hash("aa024a0244d12e60b811e0f8572825f4");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_69_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Позиции ТМЦ включенные в тариф")
                .check_md5_hash("20422a67d78a572de563083157ebede4");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_72_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Шаблоны акта выполненных работ")
                .check_md5_hash("5138bfe93d6c8ac60b6327da8126a25b");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_73_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Штрих-М (ЗАО)_Прошивка для НОВОЙ платформы ККТ и УМ/!!!201217")
                .check_md5_hash("76b331448fdf1a1f6f6a42c54bc664dd");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_74_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Штрих-М (ЗАО)_Прошивка для НОВОЙ платформы ККТ и УМ05.05.17")
                .check_md5_hash("1ce3270da29854d15cff79f3b17fdb43");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_61_step_1() {
        engineer_page.open_start_page();
        menu.download_item("PAYKIOSK_Прошивки_устранение проблемы 20122017")
                .check_md5_hash("2b0f2aacb4cbb0764069187d05d7a571");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_252_step_1() {
        engineer_page.open_start_page();
        menu.download_item("FIT_NEW_Прошивка Ритейл устранение сбоя 20.12")
                .check_md5_hash("a244c51a28e05de892167a5e40e31c98");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void T2_66_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Инструкция_Обновление ПО ККМ новой платформы Штрих через TeraTerm общая")
                .check_md5_hash("d59f948bb23104f0ba3e57e8a5846c2f");
    }
    
    @Step("При нажатии должно начаться скачивание файла")
    void LVE1_step_1() {
        engineer_page.open_start_page();
        menu.download_item("Инструкция по контрактации")
                .check_md5_hash("0cda7f81e175ae9bd1cf28c518f4d9d9");
    }
}
