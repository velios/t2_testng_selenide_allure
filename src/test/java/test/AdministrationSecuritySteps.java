package test;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static core.ApplicationManager.blocked_dummy;
import static core.ApplicationManager.return_right_element_by_cell_title;

public class AdministrationSecuritySteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Просмотр блокировок ресурсов\"")
    void T2_257_step_1() {
        system_user_page
                .open("/view-resources-lock.xhtml")
                .check_title("Просмотр блокировок ресурсов");
    }
    
    @Step("Проверить работу фильтра по всем критериям, сброс, отключение, сортировку, вернуть в исходное состояние, поделиться: генерация ссылки с состоянием, стобцы.")
    void T2_257_step_2() {
        system_user_page
                .open("/view-resources-lock.xhtml")
                .check_title("Просмотр блокировок ресурсов");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает \"Разблокировать ресурс\"")
    void T2_257_step_3() {
        blocked_dummy("Непонятно как добавить ресурс, бывает мало и нечего разблокировать");
    }
    
    @Step("Пользователь открывает страницу \"Группы безопасности\"")
    void T2_19_step_1() {
        system_user_page
                .open("/view-security-groups.xhtml")
                .check_title("Группы безопасности");
    }
    
    @Step("Пользователь выбирает в таблице группу и нажимает на Ид")
    void T2_19_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Группа безопасности");
    }
    
    @Step("Пользлватель нажимает \"Добавить запись\"")
    void T2_19_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит наименование, описание. Нажимает \"Создать\"")
    void T2_19_step_4() {
    }
    
    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_19_step_5() {
    }
    
    @Step("Пользователь выбирает группу без пользователей в таблице и нажимает \"Удалить запись\"")
    void T2_19_step_6() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Создать\"")
    void T2_20_step_1() {
        system_user_page
                .open("/view-security-groups.xhtml")
                .check_title("Группы безопасности");
        
        $(byText("Добавить новую запись")).click();
        $(byText("Создать")).click();
        
        popup
                .with_message_contains("Введите наименование группы безопасности!")
                .check_is_error();
    }
    
    @Step("Пользователь выбирает группу с пользователями и нажимает \"Удалить запись\"")
    void T2_21_step_1() {
        system_user_page
                .open("/view-security-groups.xhtml")
                .check_title("Группы безопасности");
    
        table.get_row(1).click();
        $(byText("Удалить запись")).click();
    
        popup
                .with_message_contains("Удаление невозможно! В группе есть пользователи!")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Группы ответсвенности\"")
    void T2_22_step_1() {
        system_user_page
                .open("/view-responsibility-groups.xhtml")
                .check_title("Группы ответственности");
    }
    
    @Step("Пользователь выбирает группу в таблице и нажимает на Ид")
    void T2_22_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Группа ответственности");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_22_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит наименование группы, код группы, описание группы. Нажимает \"Сохранить\"")
    void T2_22_step_4() {
    }
    
    @Step("Пользователь выбирает группу в таблице которая не используется и нажимает \"Удалить запись\"")
    void T2_22_step_5() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_23_step_1() {
        system_user_page
                .open("/view-responsibility-groups.xhtml")
                .check_title("Группы ответственности");
        
        $(byText("Добавить новую запись")).click();
        $(byText("Режим просмотра")).click();
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Введите название группы ответственности!")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Группа ответственности\"")
    void T2_17_step_1() {
        system_user_page
                .open("/responsibility-group.xhtml")
                .check_title("Группа ответственности");
    }
    
    @Step("Пользователь вводит название группы ответственности, код группы ответственности, описание групппы ответственности. Нажимает \"Сохранить\"")
    void T2_17_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь выбирает группу которая используется и нажимает \"Удалить запись\"")
    void T2_24_step_1() {
        system_user_page
                .open("/view-responsibility-groups.xhtml")
                .check_title("Группы ответственности");
        
        table.get_row(1).click();
        $(byText("Удалить запись")).click();
        
        popup
                .with_message_contains("Не допускается удаление используемых объектов")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Пользователи\"")
    void T2_32_step_1() {
        system_user_page
                .open("/view-system-users.xhtml")
                .check_title("Пользователи");
    }
    
    @Step("Пользователь нажимает \"Экспорт в Excel\"\n" +
                  "\n" +
                  "Загрузка может длится более 5 минут. Можно выбрать в фильтре одного пользователя, чтобы загрузка была быстрее.")
    void T2_32_step_2() {
        system_user_page
                .open("/view-system-users.xhtml")
                .check_title("Пользователи");
    
        table.reset_table_state();
        String first_cell_value = table.get_cell(1, "Логин").getText();
        table
                .open_filter()
                .set_value("Логин", first_cell_value)
                .submit();
        table.check_table_should_have_size(1);
    
        table.export_to_excel_download().check_file_name("Users_permissions.xlsx");
        table.reset_table_state();
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_32_step_3() {
        system_user_page
                .open("/view-system-users.xhtml")
                .check_title("Пользователи");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Пользователь");
    }
    
    @Step("Пользователь открывает страницу \"Публичные ключи\"")
    void T2_194_step_1() {
        system_user_page
                .open("/view-public-keys.xhtml")
                .check_title("Публичные ключи");
    }
    
    @Step("Пользователь выбирает ключ в таблице и нажимает на Ид")
    void T2_194_step_2() {
        system_user_page
                .open("/view-public-keys.xhtml")
                .check_title("Публичные ключи");
        table.get_cell(1, "Ид:").click();
        system_user_page.check_title("Публичный ключ");
    }
    
    @Step("Пользователь нажимает \"Загрузить новый ключ\"")
    void T2_194_step_3() {
        system_user_page
                .open("/view-public-keys.xhtml");
        $(byText("Загрузить новый ключ")).click();
    }
    
    @Step("Пользователь заполняет \"содержимое\" публичного ключа и нажимает \"Сохранить ключ\"")
    void T2_194_step_4() {
        SelenideElement save_button = $(byText("Сохранить ключ:"));
        SelenideElement textarea = return_right_element_by_cell_title("Содержимое:");
        
        save_button.shouldBe(visible);
        textarea.setValue("lveliseev_test_key");
        save_button.click();
        
        SelenideElement result_id = return_right_element_by_cell_title("Ид публичного ключа:");
        system_user_page.check_title("Публичный ключ");
        
        Allure.addAttachment("Номер созданного публичного ключа", result_id.getText());
    }
    
    @Step("Пользователь нажимает на \"Защищаемые ресурсы\"")
    void T2_243_step_1() {
        system_user_page.open_start_page();
        menu
                .download_item("Защищаемые ресурсы")
                .check_file_name("RequiresPermission.xlsx");
    }
    
    @Step("Пользователь открывает страницу \"История изминений\"")
    void T2_25_step_1() {
        system_user_page
                .open("/view-revisions.xhtml")
                .check_title("История изменений");
    }
    
    @Step("Пользователь открывает вкладку \"Пользователи\"")
    void T2_25_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь заполняет № Ревизии, тип ревизии, автор(логин), диапозон дат, логин, фамилия, класс, методы, тип доступа. Нажимает “Сформировать отчет”\n" +
                  "\n" +
                  "Для тестирования: нужно добавить себе права доступа. после сформировать отчет по себе и посмотреть что система выгружает файл с добавленными вами правами")
    void T2_25_step_3() {
    }
    
    @Step("Пользователь открывает вкладку \"Группы безопасности\"")
    void T2_25_step_4() {
    }
    
    @Step("Пользователь заполняет № Ревизии, тип ревизии, автор(логин), диапозон дат, логин, фамилия, класс, методы, тип доступа. Нажимает “Сформировать отчет”\n" +
                  "\n" +
                  "Для тестирования: выбрать опредленный период, например месяц, нажать \"Сформировать отчет\"")
    void T2_25_step_5() {
    }
}
