package test;

import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "/")
@TmsLink("1061")
public class EmployeeSectionRootTests extends EmployeeSectionRootSteps {

    @Test(description = "T2-202: Рабочий стол")
    public void T2_202() {
        T202_step_1();
    }
}
