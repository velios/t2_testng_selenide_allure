package test;

import io.qameta.allure.Step;

class NsiCommonInformationRootSteps extends SystemUserTestsBase {

    @Step("Пользователь открывает страницу \"Проекты\"")
    void T2_136_step_1() {
        system_user_page
                .open("/view-projects.xhtml")
                .check_title("Проекты");
    }

    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_136_step_2() {
        system_user_page
                .open("/view-projects.xhtml")
                .check_title("Проекты");
        
        table.export_to_excel_download().check_file_name("Projects.xlsx");
    }

    @Step("Пользователь открывает страницу \"Филиалы\"")
    void T2_139_step_1() {
        system_user_page
                .open("/view-company-branches.xhtml")
                .check_title("Филиалы");
    }

    @Step("Пользователь выбирает Филиал в таблице и нажимает на Ид")
    void T2_139_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Зоны обслуживания");
    }

    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_139_step_3() {
        system_user_page
                .open("/view-company-branches.xhtml")
                .check_title("Филиалы");

        table.export_to_excel_download().check_file_name("CompanyBranches.xlsx");
    }

    @Step("Пользователь открывает страницу \"ГеоЗоны\"")
    void T2_133_step_1() {
        system_user_page
                .open("/view-geo-zones.xhtml")
                .check_title("ГеоЗоны");
    }

    @Step("Пользователь выбирает ГеоЗону в таблице и нажимает на Ид")
    void T2_133_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Геозона");
    }

    @Step("Пользователь нажимает \"Создать зону\"")
    void T2_133_step_3() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь вводит наименование, комментарий, GeoJSON. Нажимает создать.")
    void T2_133_step_4() {
    }

    @Step("Пользователь выбирает геозону в таблице и нажимает \"Удалить запись\"")
    void T2_133_step_5() {
    }

    @Step("Пользователь открывает страницу")
    void T2_209_step_1() {
        system_user_page
                .open("/check-coords-in-geozone.xhtml")
                .check_title("Проверка вхождения координат в геозону");
    }

    @Step("Пользователь выбирает \"Тип координат\": \"OEBS A2\", \"OEBS A3\", \"Координаты\" и в зависимоти от типа, вводит необходимые данные. После чего нажимает \"Показать зоны\"")
    void T2_209_step_2() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь открывает страницу \"Группы компетенций\"")
    void T2_134_step_1() {
        system_user_page
                .open("/view-skill-groups.xhtml")
                .check_title("Группы компетенций");
    }

    @Step("Пользователь выбирает группу в таблице и нажимает на Ид")
    void T2_134_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Группа компетенций");
    }

    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_134_step_3() {
        system_user_page
                .open("/view-skill-groups.xhtml")
                .check_title("Группы компетенций");
        
        table.export_to_excel_download().check_file_name("skill-groups.xlsx");
    
        // TODO: 010 10.08.18 Добавлять скачанный файл в отчет
        // TODO: 010 10.08.18 Добавлять в специальный сьют пометку, что есть скачанные файлы
    }

    @Step("Пользователь открывает страницу \"Компетенции\"")
    void T2_135_step_1() {
        system_user_page
                .open("/view-skills.xhtml")
                .check_title("Компетенции");
    }

    @Step("Пользователь выбирает в таблице компетенцию и наждимает на Ид")
    void T2_135_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Компетенция");
    }

    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_135_step_3() {
        system_user_page
                .open("/view-skills.xhtml")
                .check_title("Компетенции");

        table.export_to_excel_download().check_file_name("skills.xlsx");
    }

    @Step("Пользователь открывает страницу \"Селекторы пользователей\"")
    void T2_164_step_1() {
        system_user_page
                .open("/view-user-selectors.xhtml")
                .check_title("Селекторы пользователей");
    }

    @Step("Пользователь выбирает селектор в таблице и нажимает на Ид")
    void T2_164_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Селектор пользователей");
    }

    @Step("Пользователь нажимает \"Добавить запись\"")
    void T2_164_step_3() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь выбирает пользователч и нажимает \"Добавить\"")
    void T2_164_step_4() {
    }

    @Step("Пользователь выбирает пользователя в селкторе и нажимает \"Удалить запись\"")
    void T2_164_step_5() {
    }

    @Step("Пользователь нажимает \"Создать\"")
    void T2_164_step_6() {
    }

    @Step("Пользователь выбирает тип и вводит название")
    void T2_164_step_7() {
    }

    @Step("Пользователь открывает страницу")
    void T2_207_step_1() {
        system_user_page
                .open("/view-oebs-project-owners.xhtml")
                .check_title("Справочник связей проектов OeBS и их владельцев");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает на Ид\n" +
            "\n" +
            "На открывшейся странице пользователь может изменить \"Проект OeBS\" и \"Владелец\"")
    void T2_207_step_2() {
        system_user_page
                .open("/view-oebs-project-owners.xhtml")
                .check_title("Справочник связей проектов OeBS и их владельцев");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Связь проектов OeBS и их владельцев");
        app.not_implemented_dummy();
    }

    @Step("Пользователь нажимает \"Создать\"")
    void T2_207_step_3() {
    }

    @Step("Пользователь выбирает \"Проект OeBS\" и \"Владелец\" и нажимает \"Сохранить\"")
    void T2_207_step_4() {
    }
}
