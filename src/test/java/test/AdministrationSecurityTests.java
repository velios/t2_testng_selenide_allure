package test;

import cfg.Constants;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "2 Безопасность")
public class AdministrationSecurityTests extends AdministrationSecuritySteps {
    
    @Test(description = "T2-19: Группы безопасности")
    @TmsLink("1142")
    public void T2_19() {
        T2_19_step_1();
        T2_19_step_2();
        T2_19_step_3();
        T2_19_step_4();
        T2_19_step_5();
        T2_19_step_6();
    }

    @Test(description = "T2-20: Группы безопасности - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1143")
    public void T2_20() {
        T2_20_step_1();
    }

    @Test(description = "T2-21:Группы безопасности - Удалить группу с пользователями")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @Issue("D-603")
    @TmsLink("1144")
    public void T2_21() {
        T2_21_step_1();
    }
    
    @Test(description = "T2-22:Группы ответственности")
    @TmsLink("1145")
    public void T2_22() {
        T2_22_step_1();
        T2_22_step_2();
        T2_22_step_3();
        T2_22_step_4();
        T2_22_step_5();
    }
    
    @Test(description = "T2-23:Группы ответственности - Пустые поля")
    @TmsLink("1146")
    public void T2_23() {
        T2_23_step_1();
    }
    
    @Test(description = "T2-17:Создать группу ответственности")
    @TmsLink("1147")
    public void T2_17() {
        T2_17_step_1();
        T2_17_step_2();
    }

    @Test(description = "T2-24:Группы ответственности - Удалить используемую группу")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @Issue("D-604")
    @TmsLink("1148")
    public void T2_24() {
        T2_24_step_1();
    }
    
    @Test(description = "Просмотр блокировок ресурсов")
    @TmsLink("1201")
    public void T2_257() {
        T2_257_step_1();
        T2_257_step_2();
        T2_257_step_3();
    }
    
    @Test(description = "T2-32:Пользователи")
    @TmsLink("1149")
    public void T2_32() {
        T2_32_step_1();
        T2_32_step_2();
        T2_32_step_3();
    }

    @Test(description = "T2-194:Публичные ключи")
    @Story(Constants.STORY_TITLE_GENERATE_CONTENT)
    @TmsLink("1150")
    public void T2_194() {
        T2_194_step_1();
        T2_194_step_2();
        T2_194_step_3();
        T2_194_step_4();
    }
    
    @Test(description = "T2-243:Защищаемые ресурсы")
    @TmsLink("1151")
    public void T2_243() {
        T2_243_step_1();
    }
    
    @Test(description = "T2-25:История изминений")
    @TmsLink("1152")
    public void T2_25() {
        T2_25_step_1();
        T2_25_step_2();
        T2_25_step_3();
        T2_25_step_4();
        T2_25_step_5();
    }

}
