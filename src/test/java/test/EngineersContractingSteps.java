package test;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

class EngineersContractingSteps extends SystemUserTestsBase {

    @Step("Пользователь открывает страницу \"Предложения о работе\"")
    void T2_161_step_1() {
        system_user_page
                .open("/view-work-offer-requests-hr.xhtml")
                .check_title("Предложения работы");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_161_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Предложение работы");
    }

    @Step("Пользователь нажимает \"Коммуникация\"")
    void T2_161_step_3() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь нажимает \"предоставить уточнение\"")
    void T2_161_step_4() {
    }

    @Step("Пользователь нажимает \"Создать предложение\" и в выдвигающемся списке выбирает \"Индивидуальное предложение\"")
    void T2_161_step_5() {
    }

    @Step("Пользователь выбирает кандидата, работодателя, тип трудоустройства, выбирает дату окончания предложения, прикладывайт файл и нажимает \"Создать и выслать\"")
    void T2_161_step_6() {
    }

    @Step("Проверить работу фильтра, сортировки, обновление страницы, ссылка.")
    void T2_161_step_7() {
    }

    @Step("Пользователь оставляет поля пустыми и нажимает \"Создать и выслать\"")
    void T2_162_step_1() {
        system_user_page
                .open("/work-offer-request-hr.xhtml")
                .check_title("Предложение работы");
        $(byText("Отправить кандидату")).click();
        popup
                .with_message_contains("Выберите кандидата")
                .check_is_error();
    }

    @Step("Пользователь открывает страницу \"Запросы инженеров\"")
    void T2_172_step_1() {
        system_user_page
                .open("/view-change-person-requests-hr.xhtml")
                .check_title("Запросы инженеров");
    }

    @Step("Пользователь выбирает запрос в статусе \"Зарегистрирован\" \"Запрос на установление трудовых отношений с работодателем\" и нажимает на Ид")
    void T2_172_step_2() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь нажимает \"Взять в обработку\"")
    void T2_172_step_3() {
    }

    @Step("Пользователь нажимает запросить уточнение, в появившейся форме вводит сообщение и нажимает \"Отправить\"")
    void T2_172_step_4() {
    }

    @Step("Пользователь нажимает кнопку \"Акцептовать\" \n" +
            "\n" +
            "Проверить что запрос выполнен, а так же в профиле инженера появился работодатель.")
    void T2_172_step_5() {
    }

    @Step("Пользователь нажимает \"Отклонить\"")
    void T2_172_step_6() {
    }

    @Step("Пользователь нажимает \"Коммуникация\"")
    void T2_172_step_7() {
    }

    @Step("Пользователь нажимает \"Добавить комментарий\", в появившемся окне вводит комментарий и нажимает \"Добавить\"")
    void T2_172_step_8() {
    }

    @Step("Пользователь открывает страницу \"Запросы на прекращение трудовых отношений\"")
    void T2_163_step_1() {
        system_user_page
                .open("/view-terminate-employment-requests-hr.xhtml")
                .check_title("Запросы на прекращение договорных отношений");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_163_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Запрос на прекращение договорных отношений");
    }

    @Step("Проверить работу фильтра, сортировки, обновление страницы, ссылка.")
    void T2_163_step_3() {
        system_user_page
                .open("/view-terminate-employment-requests-hr.xhtml")
                .check_title("Запросы на прекращение договорных отношений");
        table.reset_table_state();
    
        String first_cell_value = table.get_cell(1, "Ид").getText();
        table
                .open_filter()
                .set_value("Ид запроса", first_cell_value)
                .submit();
        table.check_table_should_have_size(1);
        table.reset_table_state();
        
        table.standart_sorting_recipe();
    }

    @Step("Пользователь открывает запрос в статусе \"Зарегистрирован\" берет в обработку, нажимает кнопку принять.")
    void T2_163_step_4() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь открывает запрос в статусе \"Зарегистрирован\" берет в обработку, нажимает кнопку \"Отклонить\"")
    void T2_163_step_5() {
    }
}
