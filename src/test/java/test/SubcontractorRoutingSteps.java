package test;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class SubcontractorRoutingSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Приоритеты правил маршрутизации на субподрядчиков\"")
    void T2_105_step_1() {
        system_user_page
                .open("/subcontractor-routing-rule-priority.xhtml")
                .check_title("Приоритеты правил маршрутизации на субподрядчиков");
    }
    
    @Step("Пользователь заполняет Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3, Геозона. Нажимает \"Сохранить\"")
    void T2_105_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь открывает страницу \"Правила маршрутизации на субподрядчиков\"")
    void T2_101_step_1() {
        system_user_page
                .open("/view-subcontractor-routing-rules.xhtml")
                .check_title("Правила маршрутизации на субподрядчиков");
    }
    
    @Step("Пользователь выбирает в таблице правила и нажимает на Ид")
    void T2_101_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Правило маршрутизации на субподрядчика");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_101_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит название правила, код правила, описание правила, дата начала, дата окончания, Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3. Геозона, компонент цены, цена, субподрядчик, группа ценнообразования.. Нажимает сохранить.")
    void T2_101_step_4() {
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_101_step_5() {
    }
    
    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_101_step_6() {
    }
    
    @Step("Пользователь нажимает \"Импорт в Excel\", в появившемся окне выбирает файл для загрузки в систему. Нажимает \"Импорт\"")
    void T2_101_step_7() {
    }
    
    @Step("Пользователь при добавлении новой записи оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_102_step_1() {
        system_user_page
                .open("/view-subcontractor-routing-rules.xhtml")
                .check_title("Правила маршрутизации на субподрядчиков");
        
        $(byText("Добавить новую запись")).click();
        $(byText("Режим просмотра")).click();
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Сохранение невозможно, укажите дату начала")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Проверка маршрутизации на субподрядчика\"")
    void T2_107_step_1() {
        system_user_page
                .open("/simulate-subcontractor-routing.xhtml")
                .check_title("Проверка маршрутизации на субподрядчика");
    }
    
    @Step("Пользователь выбирает дату для расчета, заполняет Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3, Геозона, Компонент цены. Нажимает \"Выполнить\"")
    void T2_107_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Выполнить\"")
    void T2_108_step_1() {
        system_user_page
                .open("/simulate-subcontractor-routing.xhtml")
                .check_title("Проверка маршрутизации на субподрядчика");
        
        $(byText("Выполнить")).click();
        
        popup
                .with_message_contains("Введите расчетную дату")
                .check_is_error();
    }
}
