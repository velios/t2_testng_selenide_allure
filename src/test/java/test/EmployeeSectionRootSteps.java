package test;

import io.qameta.allure.Step;

public class EmployeeSectionRootSteps extends SystemUserTestsBase {

    @Step("Пользователь открывает страницу \"Рабочий стол\"")
    void T202_step_1() {
        system_user_page.open_start_page();
    }

}
