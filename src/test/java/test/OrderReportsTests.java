package test;

import cfg.Constants;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "2 Заказ отчетов")
public class OrderReportsTests extends OrderReportsSteps {
    
    @Test(description = "T2-57:Запрос выписки")
    @TmsLink("1177")
    public void T2_57() {
        T2_57_step_1();
        T2_57_step_2();
    }
    
    @Test(description = "T2-58:Запрос информации о назначенных задачах")
    @TmsLink("1178")
    public void T2_58() {
        T2_58_step_1();
        T2_58_step_2();
    }
    
    @Test(description = "T2-59:Печать выполненных работ")
    @TmsLink("1179")
    public void T2_59() {
        T2_59_step_1();
        T2_59_step_2();
    }

    @Test(description = "T2-60:Печать выполненных работ - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1180")
    public void T2_60() {
        T2_60_step_1();
    }
}
