package test;

import cfg.Constants;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "2222 НСИ | Обработка данных | Ценообразование | Ценообразование для инженера")
public class EngineersPricingTests extends EngineersPricingSteps {
    
    @Test(description = "T2-127:Приоритет правил ценнообразования для инженеров(общий)")
    @TmsLink("1100")
    public void T2_127() {
        T2_127_step_1();
        T2_127_step_2();
        T2_127_step_3();
        T2_127_step_4();
        T2_127_step_5();
        T2_127_step_6();
        T2_127_step_7();
    }

    @Test(description = "T2-128:Приоритет правил ценнообразования для инженеров - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1101")
    public void T2_128() {
        T2_128_step_1();
    }

    @Test(description = "T2-129:Приоритет правил ценнообразования для инженеров - Удалить запись")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1102")
    public void T2_129() {
        T2_129_step_1();
    }

    @Test(description = "T2-130:Приоритет правил ценнообразования для инженеров - Сдвиг")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1103")
    public void T2_130() {
        T2_130_step_1();
        T2_130_step_2();
    }
    
    @Test(description = "T2-124:Правила ценообразования для инженеров")
    @TmsLink("1104")
    public void T2_124() {
        T2_124_step_1();
        T2_124_step_2();
        T2_124_step_3();
        T2_124_step_4();
    }

    @Test(description = "T2-126:Правило ценообразования для инженера - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1105")
    public void T2_126() {
        T2_126_step_1();
    }
    
    @Test(description = "T2-132:Проверка расчета цены для инженера")
    @TmsLink("1106")
    public void T2_132() {
        T2_132_step_1();
        T2_132_step_2();
    }
}
