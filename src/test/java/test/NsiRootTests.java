package test;

import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "2 НСИ | /")
public class NsiRootTests extends NsiRootSteps {

    @Test(description = "T2-235:Просмотр запросов (Отчеты)")
    @TmsLink("1072")
    public void T2_235(){
        T2_235_step_1();
        T2_235_step_2();
    }
    
}
