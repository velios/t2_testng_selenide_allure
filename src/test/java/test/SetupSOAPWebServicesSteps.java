package test;

import io.qameta.allure.Step;

public class SetupSOAPWebServicesSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Дескрипторы SOAP веб-сервисов\"")
    void T2_241_step_1() {
        system_user_page
                .open("/view-soap-ws-descriptors.xhtml")
                .check_title("Дескрипторы SOAP веб-сервисов");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_241_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Дескриптор SOAP веб-сервиса");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_241_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит наименование, логин и пароль проставляются автоматически. Вводит url, Тайм-аут ожидания ответа от сервера, Логирование включено, Путь до папки хранения лог-файлов веб-сервиса. Нажимает \"Сохранить\"")
    void T2_241_step_4() {
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_241_step_5() {
    }
    
    @Step("Пользователь открывает страницу \"Параметры интеграции с 1С\"")
    void T2_242_step_1() {
        system_user_page
                .open("/view-acc-integration-parameters.xhtml")
                .check_title("Параметры интеграции с 1С");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_242_step_2() {
        app.not_implemented_dummy();
    }
}
