package test;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class EngineersPricingSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Приоритет правил ценнообразования для инженеров\"")
    void T2_127_step_1() {
        system_user_page
                .open("/view-engineer-pricing-rule-priorities.xhtml")
                .check_title("Приоритет правил ценообразования для инженеров");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_127_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь заполняет все галочки, выбирает компонент цены. Нажимает \"Сохранить\"")
    void T2_127_step_3() {
    }
    
    @Step("Пользователь выбирает приоритет в таблице и нажимает на Ид")
    void T2_127_step_4() {
    }
    
    @Step("Пользователь выбирает приоритет в таблице и нажимает \"Удалить запись\"")
    void T2_127_step_5() {
    }
    
    @Step("Пользователь выбирает приоритет в таблице и нажимает \"Сдвинуть вверх\"")
    void T2_127_step_6() {
    }
    
    @Step("Пользователь выбирает приоритет в таблице и нажимает \"Сдвинуть вниз\"")
    void T2_127_step_7() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_128_step_1() {
        system_user_page
                .open("/view-engineer-pricing-rule-priorities.xhtml")
                .check_title("Приоритет правил ценообразования для инженеров");
        
        $(byText("Добавить новую запись")).click();
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Укажите компонент цены")
                .check_is_error();
    }
    
    @Step("Пользователь нажимает \"Удалить запись\" не выбирая запись в таблице")
    void T2_129_step_1() {
        system_user_page
                .open("/view-engineer-pricing-rule-priorities.xhtml")
                .check_title("Приоритет правил ценообразования для инженеров");
        
        $(byText("Удалить запись")).click();
        
        popup
                .with_message_contains("Пожалуйста, выберите запись для удаления")
                .check_is_error();
    }
    
    @Step("Пользователь нажимает на кнопку “Сдвинуть вверх” не выбирая запись")
    void T2_130_step_1() {
        system_user_page
                .open("/view-engineer-pricing-rule-priorities.xhtml")
                .check_title("Приоритет правил ценообразования для инженеров");
        
        $(byText("Сдвинуть вверх")).click();
        
        popup
                .with_message_contains("Выберите запись")
                .check_is_error();
    }
    
    @Step("Пользователь нажимает на кнопку “Сдвинуть вниз” не выбирая запись")
    void T2_130_step_2() {
        system_user_page
                .open("/view-engineer-pricing-rule-priorities.xhtml")
                .check_title("Приоритет правил ценообразования для инженеров");
        
        $(byText("Сдвинуть вниз")).click();
        
        popup
                .with_message_contains("Выберите запись")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Правила ценообразования для инженеров\"")
    void T2_124_step_1() {
        system_user_page
                .open("/view-engineer-pricing-rules.xhtml")
                .check_title("Правила ценообразования для инженеров");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_124_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь заполняет дата начала, дата окончания, Oebs Ид филиала, Oebs код зоны, Oebs Ид заказчика, Oebs код проекта, Oebs Ид продукта,Oebs Ид производителя, Oebs Ид услуги, Oebs Ид A2, Oebs Ид A3, Название геозоны, Цена, Название компонента цены. Нажимает \"Сохранить\"")
    void T2_124_step_3() {
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_124_step_4() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Удалить запись\"")
    void T2_126_step_1() {
        system_user_page
                .open("/engineer-pricing-rule.xhtml")
                .check_title("Правило ценообразования для инженера");
        
        $(byText("Режим просмотра")).click();
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Сохранение невозможно, укажите дату начала")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Проверка расчета цены для инженера\"")
    void T2_132_step_1() {
        system_user_page
                .open("/simulate-engineer-price-calculation.xhtml")
                .check_title("Проверка расчета цены для инженера");
    }
    
    @Step("Пользователь выбирает дату для расчета. Заполняет Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3, Геозона, Компонент цены. Нажимает \"Выполнить\"")
    void T2_132_step_2() {
        app.not_implemented_dummy();
    }
}
