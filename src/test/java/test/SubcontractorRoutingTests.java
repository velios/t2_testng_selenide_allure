package test;


import cfg.Constants;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "221 НСИ | Обработка данных | Маршрутизация на субподрядчиков")
public class SubcontractorRoutingTests extends SubcontractorRoutingSteps {
    
    @Test(description = "T2-105:Приоритеты правил маршрутизации на субподрядчиков")
    @TmsLink("1085")
    public void T2_105() {
        T2_105_step_1();
        T2_105_step_2();
    }

    @Test(description = "T2-101:Правила маршрутизации на субподрядчиков")
    @Issue("D-610")
    @Issue("D-589")
    @TmsLink("1086")
    public void T2_101() {
        T2_101_step_1();
        T2_101_step_2();
        T2_101_step_3();
        T2_101_step_4();
        T2_101_step_5();
        T2_101_step_6();
        T2_101_step_7();
    }

    @Test(description = "T2-102:Правила маршрутизации на субподрядчиков - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1087")
    public void T2_102() {
        T2_102_step_1();
    }
    
    @Test(description = "T2-107:Проверка маршрутизации на субподрядчика")
    @TmsLink("1088")
    public void T2_107() {
        T2_107_step_1();
        T2_107_step_2();
    }

    @Test(description = "T2-108:Проверка маршрутизации на субподрядчика - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1089")
    public void T2_108() {
        T2_108_step_1();
    }

}
