package test;

import org.testng.annotations.BeforeTest;

public class SystemUserTestsBase extends TestBase {

    @BeforeTest(description = "Авторизация системного пользователя", alwaysRun = true)
    public void testSetUp() {
        app.system_user_tests_init();
    }
}
