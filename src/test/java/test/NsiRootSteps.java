package test;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import page_objects.MainTable;

import static com.codeborne.selenide.Selenide.$;

public class NsiRootSteps extends SystemUserTestsBase {
    
    
    @Step("Пользователь открывает страницу \"Отчёты\"")
    void T2_235_step_1() {
        system_user_page
                .open("/view-import-export-jobs.xhtml")
                .check_title("Отчёты");
//        app.getTableHelper().check_text_in_table_headers(
//                "Ид",
//                "Посл. изм.",
//                "Время регистрации",
//                "Время выполнения",
//                "Тип отчета",
//                "Статус",
//                "Результат",
//                "Ошибки"
//        );
        new MainTable().check_headers_text(
                "Ид",
                "Посл. изм.",
                "Время регистрации",
                "Время выполнения",
                "Тип отчета",
                "Статус",
                "Результат",
                "Ошибки"
        );
    }
    
    @Step("Пользователь в столбце \"Статус\", нажимает на кнопку \"Скачать\"")
    void T2_235_step_2() {
        SelenideElement first_row_download_button = $("#form\\:idMainDataTable\\:0\\:btnDownloadResult");
        system_user_page
                .download_element(first_row_download_button)
                .check_file_extension("xlsx");
    }
}
