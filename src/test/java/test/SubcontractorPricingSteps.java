package test;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class SubcontractorPricingSteps extends SystemUserTestsBase {
    
    
    @Step("Пользователь откывает страницу \"Приоритет правил ценообразования для субподрядчиков\"")
    void T2_121_step_1() {
        system_user_page
                .open("/view-subcontractor-pricing-rule-priorities.xhtml")
                .check_title("Приоритет правил ценообразования для субподрядчиков");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_121_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит Oebs филиал\u200B, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs А3, Геозона, Группа ценообразования, Субподрядчик. Выбрать компонент цены. Нажимает сохранить.")
    void T2_121_step_3() {
    }
    
    @Step("Пользователь выбирает приоритет в таблице и нажимает \"Удалить запись\"")
    void T2_121_step_4() {
    }
    
    @Step("Пользователь выбирает приоритет в таблице и нажимает на Ид")
    void T2_121_step_5() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_120_step_1() {
        system_user_page
                .open("/subcontractor-pricing-rule-priority.xhtml")
                .check_title("Приоритет правил ценообразования для субподрядчика");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Укажите компонент цены")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Правила ценообразования для субподрядчиков\"")
    void T2_115_step_1() {
        system_user_page
                .open("/view-subcontractor-pricing-rules.xhtml")
                .check_title("Правила ценообразования для субподрядчиков");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_115_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Правило ценообразования для субподрядчика");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_115_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит название правила, код правила, описание правила, дата начала, дата окончания, Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3. Геозона, компонент цены, цена, субподрядчик, группа ценнообразования.. Нажимает сохранить.")
    void T2_115_step_4() {
    }
    
    @Step("Пользователь выбирает правило в таблице и нажимает \"Удалить запись\"")
    void T2_115_step_5() {
    }
    
    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_115_step_6() {
    }
    
    @Step("Пользователь нажимает \"Импорт в Excel\". В появмшемся окне выбирает файл на своем компьютере и нажимает \"Импорт\"")
    void T2_115_step_7() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_116_step_1() {
        system_user_page
                .open("/view-subcontractor-pricing-rules.xhtml")
                .check_title("Правила ценообразования для субподрядчиков");
        
        $(byText("Добавить новую запись")).click();
        $(byText("Режим просмотра")).click();
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Сохранение невозможно, укажите дату начала")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Проверка расчета цены для субподрядчика\"")
    void T2_122_step_1() {
        system_user_page
                .open("/simulate-subcontractor-price-calculation.xhtml")
                .check_title("Проверка расчета цены для субподрядчика");
    }
    
    @Step("Пользователь выбирает дату для расчета, заполняет Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3, Геозона, Компонент цены. Нажимает кнопку \"Выполнить\"")
    void T2_122_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Выполнить\"")
    void T2_123_step_1() {
        system_user_page
                .open("/simulate-subcontractor-price-calculation.xhtml")
                .check_title("Проверка расчета цены для субподрядчика");
        
        $(byText("Выполнить")).click();
        
        popup
                .with_message_contains("Введите расчетную дату")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Группы ценообразования\"")
    void T2_113_step_1() {
        system_user_page
                .open("/view-pricing-groups.xhtml")
                .check_title("Группы ценообразования");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_113_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Группа ценообразования");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_113_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит название группы ценообразования, описание группы ценообразования. Нажимает создать")
    void T2_113_step_4() {
    }
    
    @Step("Пользователь выбирает группу в таблице и нажимает \"Удалить запись\"")
    void T2_113_step_5() {
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Создать\"")
    void T2_114_step_1() {
        system_user_page
                .open("/view-pricing-groups.xhtml")
                .check_title("Группы ценообразования");
        
        $(byText("Добавить новую запись")).click();
        $(byText("Создать")).click();
        
        popup
                .with_message_contains("Введите название группы ценообразования")
                .check_is_error();
    }
}
