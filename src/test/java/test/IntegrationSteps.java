package test;

import cfg.Constants;
import com.codeborne.selenide.SelenideElement;
import core.ApplicationManager;
import io.qameta.allure.Step;
import page_objects.Dropdown;

import java.io.File;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class IntegrationSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Инциденты\"")
    void T2_83_step_1() {
        system_user_page
                .open("/view-incidents.xhtml")
                .check_title("Инциденты");
    }
    
    @Step("Пользователь выбирает инцидент в таблице и нажимает на Ид")
    void T2_83_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Инцидент");
    }
    
    @Step("Пользователь открывает страницу \"Запросы\"")
    void T2_81_step_1() {
        system_user_page
                .open("/view-requests-hr.xhtml")
                .check_title("Запросы");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_81_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_h1_header_contains(
                "Запрос на",
                "Анализ дерева запросов");
    }
    
    @Step("Пользователь открывает страницу \"Создание JMS сообщений\"")
    void T2_236_step_1() {
        system_user_page
                .open("/create-jms-message-view.xhtml")
                .check_title("Создание JMS сообщений");
    }
    
    @Step("Пользователь выбирает очередь сообщений из представленного списка, прикладывает файл. Нажимает \"Послать сообщение\"")
    void T2_236_step_2() {
        system_user_page
                .open("/create-jms-message-view.xhtml")
                .check_title("Создание JMS сообщений");
        SelenideElement select_queue_dropdown = $("#form\\:queueType");
        SelenideElement upload_button = $("#form\\:uploadFile");
        SelenideElement upload_input = $("#form\\:jmsFileUploadFile_input");
        SelenideElement submit_button = $(byText("Послать сообщение"));
        
        Dropdown dropdown = new Dropdown(select_queue_dropdown);
        dropdown.choose_last_option();
        upload_button.click();
        // TODO: 009 09.08.18 Переписать c Extension Methods http://manifold.systems/
        upload_input.uploadFile(new File(cfg.sample_txt_file_path()));
        
        submit_button.click();
        popup
                .with_message_contains("Сообщение успешно отправлено")
                .check_is_info();
    }
    
    @Step("Пользователь открывает страницу \"JMS очереди\"")
    void T2_237_step_1() {
        system_user_page
                .open("/view-jms-queues.xhtml")
                .check_title("JMS очереди");
    }
    
    @Step("Пользователь выбирает очередь в таблице и нажимает на нее")
    void T2_237_step_2() {
        system_user_page
                .open("/view-jms-queues.xhtml")
                .check_title("JMS очереди");
        // TODO: 019 19.08.18 Таблица называется не form:idMainDataTable как обычно, а form:queueInfosDataTable. Придумать способ работать с таблицей с нестандартным названием
//        table.get_cell(1, "Название очереди").click();
//        system_user_page.check_h1_header_contains("JMS очередь java");
        app.not_implemented_dummy();
    }
    
    @Step("В таблице пользователь выбирает запись и нажимает на Ид")
    void T2_237_step_3() {
    }
    
    @Step("Пользователь может нажать: \"Сохранить на диск\" или \"Послать заново\" или \"Закрыть вручную\"")
    void T2_237_step_4() {
    }
    
    @Step("Пользователь открывает страницу \"Параметры JMS очередей\"")
    void T2_84_step_1() {
        system_user_page
                .open("/view-queue-parameters.xhtml")
                .check_title("Параметры JMS очередей");
    }
    
    @Step("Пользователь нажимает \"Добавить запись\"")
    void T2_84_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь заполняет все поля и нажимает \"Создать\"")
    void T2_84_step_3() {
    }
    
    @Step("Пользователь выбирает параметр в таблице и нажимает \"Удалить запись\"")
    void T2_84_step_4() {
    }
    
    @Step("Пользователь выбирает параметр в таблице и нажимает на Ид")
    void T2_84_step_5() {
    }
    
    @Step("Пользователь открывает страницу \"Диагностика вызовов\"")
    void T2_80_step_1() {
        system_user_page
                .open("/view-diagnostic-entries.xhtml")
                .check_title("Диагностика вызовов");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_80_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Интеграционный запрос");
    }
    
    @Step("Пользователь открывает страницу \"Сырые\" данные по актам выполненных работ\"")
    void T2_238_step_1() {
        system_user_page
                .open("/view-oebs-works-completed-raw.xhtml")
                .check_title("\"Сырые\" данные по актам выполненных работ");
    }
    
    @Step("Пользователь открывает страницу и нажимает на Ид")
    void T2_238_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Акт выполненных работ (\"сырые\" данные)");
    }
    
    @Step("Так же пользователь может перейти из таблицы сразу в JMS-сообщение нажав на Ид\n" +
                  "\n" +
                  "В JMS сообщение есть доступные кнопки: \"Сохранить на диск\", \"Послать заново\", \"Закрыть вручную\"")
    void T2_238_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь открывает страницу \"Заказ-наряды\"")
    void T2_258_step_1() {
        system_user_page
                .open("/view-oebs-work-orders.xhtml")
                .check_title("Заказ-наряды");
    }
    
    @Step("Проверить работу фильтра, сброса фильтра и отключения. Работу сортировки, вернуть состояние в исходное, поделиться, столбцы, переключение страниц.")
    void T2_258_step_2() {
        system_user_page
                .open("/view-oebs-work-orders.xhtml")
                .check_title("Заказ-наряды");
        table.reset_table_state();
        
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид\n" +
                  "Проверить кликабельность Исполнителя в заказ-наряде")
    void T2_258_step_3() {
        system_user_page
                .open("/view-oebs-work-orders.xhtml")
                .check_title("Заказ-наряды");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Заказ-наряд");
    }
    
    @Step("Пользователь открывает страницу \"Некорректно импортированные заказы-наряды\"")
    void T2_259_step_1() {
        system_user_page
                .open("/view-oebs-work-order-import-errors.xhtml")
                .check_title("Некорректно импортированные заказы-наряды");
    }
    
    @Step("Проверить работу фильтра, сброса фильтра и отключения. Работу сортировки, вернуть состояние в исходное, поделиться, столбцы, переключение страниц.")
    void T2_259_step_2() {
        ApplicationManager.blocked_dummy(Constants.DUMMY_NOT_DATA_IN_TABLE);
    }
    
    @Step("Пользователь открывает Некорректно импортированный заказ-наряд нажимая на Ид")
    void T2_259_step_3() {
    }
    
    @Step("Пользователь может указать \"Принципала\" и \"Субподрядчика\" и нажать \"Сохранить\" ")
    void T2_259_step_4() {
    }
}
