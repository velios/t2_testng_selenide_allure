package test;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class PricingRootSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Компонент цены\"")
    void T2_109_step_1() {
        system_user_page
                .open("/price-component.xhtml")
                .check_title("Компонент цены");
    }
    
    @Step("Пользователь заполняет название компонента, код компонента, описание компонента, активный. Нажимает \"Сохранить\"")
    void T2_109_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_110_step_1() {
        system_user_page
                .open("/price-component.xhtml")
                .check_title("Компонент цены");
        
        $(byText("Режим просмотра")).click();
        $(byText("Создать")).click();
        
        popup
                .with_message_contains("Введите название")
                .check_is_error();
    }
}
