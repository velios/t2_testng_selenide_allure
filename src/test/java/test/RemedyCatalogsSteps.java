package test;

import io.qameta.allure.Step;

public class RemedyCatalogsSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Группы поддержки Remedy\"\n" +
                  "\n" +
                  "Столбццы в таблице: Ид, Ид группы поддержки, Наименование группы поддержки, Статус.")
    void T2_233_step_1() {
        system_user_page
                .open("/view-remedy-support-groups.xhtml")
                .check_title("Группы поддержки Remedy");
        table.check_headers_text("ИД", "Дата создания", "Дата изменения", "Ид группы поддержки", "Наименование группы поддержки", "Статус", "Ид филиала", "Наименование филиала", "Статус");
        
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_233_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Группа поддержки Remedy (подробные данные)");
    }
    
    @Step("Пользователь открывает страницу \"Карточки пользователей Remedy\"\n" +
                  "\n" +
                  "Столбцы в таблице: Ид, Логин, Имя, Фамилия, Статус.")
    void T2_234_step_1() {
        system_user_page
                .open("/view-remedy-people.xhtml")
                .check_title("Карточки пользователей Remedy");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Логин", "Имя", "Фамилия", "Статус remedy");
        
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_234_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Карточка пользователя Remedy (подробные данные)");
    }
}
