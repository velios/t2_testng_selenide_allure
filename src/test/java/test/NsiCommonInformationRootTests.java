package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "21 НСИ | Общие данные | /")
class NsiCommonInformationRootTests extends NsiCommonInformationRootSteps {
    
    @Test(description = "T2-136:Проекты")
    @TmsLink("1073")
    public void T2_136() {
        T2_136_step_1();
        T2_136_step_2();
    }
    
    @Test(description = "T2-139:Филиалы")
    @TmsLink("1074")
    public void T2_139() {
        T2_139_step_1();
        T2_139_step_2();
        T2_139_step_3();

    }
    
    @Test(description = "T2-133:ГеоЗоны")
    @TmsLink("1075")
    public void T2_133() {
        T2_133_step_1();
        T2_133_step_2();
        T2_133_step_3();
        T2_133_step_4();
        T2_133_step_5();
    }
    
    @Test(description = "T2-209:Проверка вхождения координат в геозону")
    @TmsLink("1076")
    public void T2_209() {
        T2_209_step_1();
        T2_209_step_2();
    }
    
    @Test(description = "T2-134:Группы компетенций")
    @TmsLink("1077")
    public void T2_134() {
        T2_134_step_1();
        T2_134_step_2();
        T2_134_step_3();
    }
    
    @Test(description = "T2-135:Компетенции")
    @TmsLink("1078")
    public void T2_135() {
        T2_135_step_1();
        T2_135_step_2();
        T2_135_step_3();
    }

    @Test(description = "T2-164:Селекторы пользователей")
    @Issue("D-618")
    @Issue("D-636")
    @Issue("D-638")
    @TmsLink("1079")
    public void T2_164() {
        T2_164_step_1();
        T2_164_step_2();
        T2_164_step_3();
        T2_164_step_4();
        T2_164_step_5();
        T2_164_step_6();
        T2_164_step_7();
    }
    
    @Test(description = "T2-207: Справочник связей проектов OeBS и их владельцев")
    @TmsLink("1080")
    public void T2_207() {
        T2_207_step_1();
        T2_207_step_2();
        T2_207_step_3();
        T2_207_step_4();
    }
}
