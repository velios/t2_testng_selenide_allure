package test;

import cfg.Constants;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "222 НСИ | Обработка данных | Ценообразование | /")
public class PricingRootTests extends PricingRootSteps {
    
    @Test(description = "T2-109:Компонент цены")
    @TmsLink("1090")
    public void T2_109() {
        T2_109_step_1();
        T2_109_step_2();
    }

    @Test(description = "T2-110:Компонент цены - Пустые поля")
    @Story(Constants.STORY_TITLE_ERROR_POPUP_IS_DISPLAYED)
    @TmsLink("1091")
    public void T2_110() {
        T2_110_step_1();
    }
}
