package test;

import org.testng.annotations.BeforeTest;

public class EngineerTestsBase extends TestBase {

    @BeforeTest(description = "Авторизация инженера", alwaysRun = true)
    public void testSetUp() {
        app.engineer_tests_init();
    }
}
