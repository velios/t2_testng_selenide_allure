package test;

import io.qameta.allure.Step;

public class CompetencyRequirementsSteps extends SystemUserTestsBase {

    @Step("Пользователь открывает страницу \"Приоритеты правил требования компетенций\"")
    void T2_217_step_1() {
        system_user_page
                .open("/view-required-skills-selection-rule-priorities.xhtml")
                .check_title("Приоритеты правил требования компетенций");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_217_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Приоритет правил требования компетенции");
    }

    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_217_step_3() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь заполняет ячейки: Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs модель, Oebs производитель, Oebs услуга, Oebs A2, Oebs А3, Геозона, Фаза. Нажимает \"Сохранить\"")
    void T2_217_step_4() {
    }

    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_217_step_5() {
    }

    @Step("Пользователь выбирает запись в таблице и нажимает \"Сдвинуть вверх\"")
    void T2_217_step_6() {
    }

    @Step("Пользователь выбирает запись в таблице и нажимает \"Сдвинуть вниз\"")
    void T2_217_step_7() {
    }

    @Step("Пользователь открывает страницу \"Правила требования компетенций\"")
    void T2_218_step_1() {
        system_user_page
                .open("/view-required-skills-selection-rules.xhtml")
                .check_title("Правила требования компетенций");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_218_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Правило требования компетенций");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_218_step_3() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_218_step_4() {
    }

    @Step("Пользователь вводит: название правила, код правила, описание правила, вариант ввода, дата начала, дата окончания, Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs модель, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3, Геозона, Фаза. Так же пользователь может добавить или удалить комптенции к правилу. После чего нажимает \"Сохранить\" или \"Создать еще одно правило\"")
    void T2_218_step_5() {
    }

    @Step("Пользователь нажимает \"Экспорт в Excel\"")
    void T2_218_step_6() {
    }

    @Step("Пользователь нажимает \"Импорт в Excel\", открывается окно в котором пользователь выбирает файл на компьютере и нажимает \"Импорт\"")
    void T2_218_step_7() {
    }

    @Step("Пользователь открывает страницу")
    void T2_219_step_1() {
        system_user_page
                .open("/view-required-skills-selection-phases.xhtml")
                .check_title("Фазы выбора компетенций");
    }

    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_219_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Фаза выбора компетенций");
    }

    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_219_step_3() {
        app.not_implemented_dummy();
    }

    @Step("Пользователь вводит код, наименование, описание. Нажимает \"Сохранить\"")
    void T2_219_step_4() {
    }

    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_219_step_5() {
    }

    @Step("Пользователь открывает страницу \"Проверка требуемых компетенций\"")
    void T2_220_step_1() {
        system_user_page
                .open("/simulate-required-skills.xhtml")
                .check_title("Проверка требуемых компетенций");
    }

    @Step("Пользователь выбирает вариант ввода: \"Инцидент по А2\" или \"Индидент по А3\". Вводит данные: дата для расчета, Oebs филиал, Oebs зона, Oebs заказчик, Oebs проект, Oebs продукт, Oebs модель, Oebs производитель, Oebs услуга, Oebs A2, Oebs A3, Геозона, Работодатель, Фаза. Нажимает \"Выполнить\".")
    void T2_220_step_2() {
        app.not_implemented_dummy();
    }
}
