package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "3 Интеграция")
public class IntegrationTests extends IntegrationSteps {
    
    @Test(description = "T2-83:Инциденты")
    @TmsLink("1127")
    public void T2_83() {
        T2_83_step_1();
        T2_83_step_2();
    }
    
    @Test(description = "T2-81:Запросы")
    @TmsLink("1128")
    public void T2_81() {
        T2_81_step_1();
        T2_81_step_2();
    }
    
    @Test(description = "T2-236:Отправить JMS сообщение")
    @TmsLink("1129")
    public void T2_236() {
        T2_236_step_1();
        T2_236_step_2();
    }
    
    @Test(description = "T2-237:JMS очереди")
    @TmsLink("1130")
    public void T2_237() {
        T2_237_step_1();
        T2_237_step_2();
        T2_237_step_3();
        T2_237_step_4();
    }
    
    @Test(description = "T2-84:Параметры JMS очередей")
    @TmsLink("1131")
    public void T2_84() {
        T2_84_step_1();
        T2_84_step_2();
        T2_84_step_3();
        T2_84_step_4();
        T2_84_step_5();
    }

    @Test(description = "T2-80:Диагностика вызовов")
    @Issue("D-524")
    @TmsLink("1132")
    public void T2_80() {
        T2_80_step_1();
        T2_80_step_2();
    }
    
    @Test(description = "T2-238:\"Сырые\" данные по актам выполненных работ")
    @TmsLink("1133")
    public void T2_238() {
        T2_238_step_1();
        T2_238_step_2();
        T2_238_step_3();
    }
    
    @Test(description = "T2-258:Заказ-наряды")
    public void T2_258() {
        // TODO: 020 20.08.18 Дописать TmsLink, как только станет понятен номер
        T2_258_step_1();
        T2_258_step_2();
        T2_258_step_3();
    }
    
    @Test(description = "T2-259:Некорректно импортированные заказы-наряды")
    public void T2_259() {
        // TODO: 020 20.08.18 Дописать TmsLink, как только станет понятен номер
        T2_259_step_1();
        T2_259_step_2();
        T2_259_step_3();
        T2_259_step_4();
    }
}
