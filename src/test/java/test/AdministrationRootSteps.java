package test;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import page_objects.MainTable;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public class AdministrationRootSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Системные параметры\"")
    void T2_239_step_1() {
        system_user_page
                .open("/system-param.xhtml")
                .check_title("Системные параметры");
    }
    
    @Step("Пользователь может изменить любое поле в таблице. После чего нажимает \"Сохранить\"")
    void T2_239_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь открывает страницу \"Системные уведомления\"")
    void T2_240_step_1() {
        system_user_page
                .open("/view-managed-user-notifications.xhtml")
                .check_title("Системные уведомления");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_240_step_2() {
        new MainTable().get_cell(1, "Ид").click();
        system_user_page.check_title("Уведомление");
    }
    
    @Step("Пользователь нажимает \"Отправить уведомление\"")
    void T2_240_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь выбирает кому отправить, вводит заголовок, текст сообщения, выбирает требует подтверждения, продублировать сообщение по email. Нажимает \"Отправить\"")
    void T2_240_step_4() {
    }
    
    @Step("Пользователь открывает страницу \"СМС сообщения\"")
    void T2_195_step_1() {
        system_user_page
                .open("/view-admin-sms.xhtml")
                .check_title("Системные СМС");
    }
    
    @Step("Пользователь выбирает сообщение в таблице и нажимает на Ид")
    void T2_195_step_2() {
        system_user_page
                .open("/view-admin-sms.xhtml")
                .check_title("Системные СМС");
        // FIXME: 019 19.08.18 Ошибка с получением ячейки, исправить
        table.get_cell(1, "Ид:").click();
        system_user_page.check_title("Системное СМС");
    }
    
    @Step("Пользователь нажимает \"Отправить СМС сообщение\"")
    void T2_195_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит номер адресата и сообщение и нажимает \"Отправить СМС\", система по умолчанию использует последний публичный ключ.")
    void T2_195_step_4() {
    }
    
    @Step("Пользователь открывает страницу \"Журнал аудита\"")
    void T2_12_step_1() {
        system_user_page
                .open("/view-audit.xhtml")
                .check_title("Журнал аудита");
    }
    
    @Step("Пользователь нажимает на \"Экспорт в Excel\"")
    void T2_12_step_2() {
        SelenideElement export_to_excel_button = $(byText("Экспорт в Excel"));
        export_to_excel_button.click();
        
        SelenideElement popup_window = $(".ui-growl-image-info");
        popup_window.shouldBe(visible);
    }
    
    @Step("Пользователь открывает страницу \"Периодические задания\"")
    void T2_14_step_1() {
        system_user_page
                .open("/view-job-schedules.xhtml")
                .check_title("Периодические задания");
    }
    
    @Step("Пользователь выбирает в таблице задание и нажимает на Ид")
    void T2_14_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Периодические задания");
    }
    
    @Step("Пользователь открывает страницу \"Доверенные отправители АВР\"")
    void T2_244_step_1() {
        system_user_page
                .open("/view-trusted-email-sender.xhtml")
                .check_title("Доверенные отправители АВР");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_244_step_2() {
        system_user_page
                .open("/view-trusted-email-sender.xhtml")
                .check_title("Доверенные отправители АВР");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Доверенный отправитель АВР");
    }
    
    @Step("Пользователь нажимает \"Добавить новую запись\"")
    void T2_244_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь вводит e-mail, нажимает \"Сохранить\"")
    void T2_244_step_4() {
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает \"Удалить запись\"")
    void T2_244_step_5() {
    }
}
