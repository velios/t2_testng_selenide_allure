package test;

import core.ApplicationManager;
import core.SortingOptions;
import io.qameta.allure.Step;

public class OebsCatalogsSteps extends SystemUserTestsBase {
    
    @Step("Пользователь открывает страницу \"Справочник А3\"")
    void T2_249_step_1() {
        system_user_page
                .open("/view-oebs-a3.xhtml")
                .check_title("Справочник А3");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Ид А3", "Номер А3", "Ид продукта",
                                 "Наименование продукта", "Ид производителя", "Наименование производителя", "Модель",
                                 "Ид родительского актива", "Серийный номер");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_249_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs А3");
    }
    
    @Step("Проверить работу фильтра, сортировки, обновление страницы, ссылка.")
    void T2_249_step_3() {
        system_user_page
                .open("/view-oebs-a3.xhtml")
                .check_title("Справочник А3");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник A2\"")
    void T2_250_step_1() {
        system_user_page
                .open("/view-oebs-a2.xhtml")
                .check_title("Справочник А2");
        table.check_headers_text("Ид", "Ид А2", "Номер А2", "Ид организации", "Код oebs зоны", "Адрес", "Статус",
                                 "Дата создания");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_250_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs А2");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_250_step_3() {
        system_user_page
                .open("/view-oebs-a2.xhtml")
                .check_title("Справочник А2");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Польхзователь открывает страницу \"Справочник сервисов\"")
    void T2_221_step_1() {
        system_user_page
                .open("/view-oebs-services.xhtml")
                .check_title("Справочник сервисов");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Наменование",
                                 "Код", "Ид сервиса");
    }
    
    @Step("Пользователь выбирает  запись в таблице и нажимает на Ид")
    void T2_221_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs Сервис");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_221_step_LVE1() {
        system_user_page
                .open("/view-oebs-services.xhtml")
                .check_title("Справочник сервисов");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("ПОльзователь открывает страницу")
    void T2_222_step_1() {
        system_user_page
                .open("/view-oebs-product.xhtml")
                .check_title("Справочник продуктов");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания",
                                 "Наименование продукта", "Ид продукта", "Код продукта");
        
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs Сервис");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_222_step_LVE1() {
        system_user_page
                .open("/view-oebs-product.xhtml")
                .check_title("Справочник продуктов");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник производителей\"\n" + "\n"
                  + "Пользователь ввидит в таблице: Ид, наименование, Ид производителя.")
    void T2_223_step_1() {
        system_user_page
                .open("/view-oebs-manufacturer.xhtml")
                .check_title("Справочник производителей");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Наименование",
                                 "Ид производителя");
        
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs производитель");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_223_step_LVE1() {
        system_user_page
                .open("/view-oebs-manufacturer.xhtml")
                .check_title("Справочник производителей");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник моделей\"\n" + "\n"
                  + "Пользователь видит Ид, модель, ид продукта, ид производителя, описание.")
    void T2_224_step_1() {
        system_user_page
                .open("/view-oebs-model.xhtml")
                .check_title("Справочник моделей");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Модель",
                                 "Ид продукта", "Ид производителя", "Описание");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_224_step_LVE1() {
        system_user_page
                .open("/view-oebs-model.xhtml")
                .check_title("Справочник моделей");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник заказчиков\"\n" + "\n"
                  + "Пользователь может посмотреть: Ид, наименование заказчика, ид заказчика.")
    void T2_225_step_1() {
        system_user_page
                .open("/view-oebs-customers.xhtml")
                .check_title("Справочник заказчиков");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания",
                                 "Наименование заказчика", "Ид заказчика");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_225_step_LVE1() {
        system_user_page
                .open("/view-oebs-customers.xhtml")
                .check_title("Справочник заказчиков");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник проектов\"")
    void T2_226_step_1() {
        system_user_page
                .open("/view-oebs-projects.xhtml")
                .check_title("Справочник проектов");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Код",
                                 "Наименование",
                                 "Наличие допустимых исполнителей");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид")
    void T2_226_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Проект OeBS");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_226_step_LVE1() {
        system_user_page
                .open("/view-oebs-projects.xhtml")
                .check_title("Справочник проектов");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник организаций\"\n" + "\n"
                  + "Пользователь может посмотреть: Ид, ид организации, код организации, наименовании организации.")
    void T2_227_step_1() {
        system_user_page
                .open("/view-oebs-org.xhtml")
                .check_title("Справочник организаций");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания",
                                 "Ид организации", "Код организации",
                                 "Наименование организации");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_227_step_LVE1() {
        system_user_page
                .open("/view-oebs-org.xhtml")
                .check_title("Справочник организаций");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу \"Справочник зон\"\n" + "\n"
                  + "Пользователь может посмотреть: Ид, наименование зоны, код зоны, ид организации, начало активного периода, конец активного периода, статус зоны.")
    void T2_228_step_1() {
        system_user_page
                .open("/view-oebs-zone.xhtml")
                .check_title("Справочник зон");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания",
                                 "Наименование зоны", "Код зоны", "Ид организации",
                                 "Начало активного периода", "Конец активного периода");
        
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs зона");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_228_step_LVE1() {
        system_user_page
                .open("/view-oebs-zone.xhtml")
                .check_title("Справочник зон");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открывает страницу Договоры\n" + "\n"
                  + "Пользователь может посмотреть: Ид, ид договора, номер договора, ид заказчика, код проекта, дата начала действия договора, дата окончания дейсвтвия договора, статус.")
    void T2_229_step_1() {
        system_user_page
                .open("/view-oebs-agreements.xhtml")
                .check_title("Договора");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания", "Ид договора",
                                 "Номер договора", "Ид заказчика", "Код проекта",
                                 "Дата начала действия договора",
                                 "Дата окончания действия договора");
        
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Детали договора");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_229_step_LVE1() {
        system_user_page
                .open("/view-oebs-agreements.xhtml")
                .check_title("Договора");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользовватель открывает страницу \"Показ детальных условий договоров\"")
    void T2_255_step_1() {
        system_user_page
                .open("/view-oebs-agreement-detail.xhtml")
                .check_title("Показ детальных условий договоров");
    }
    
    @Step("Проверить работу фильтра по всем параметрам, сброс и отключение фильтра. Использование сортировки. Вернуть состояние в исходное. Кнопка \"Поделиться\" и \"Столбцы\"")
    void T2_255_step_2() {
        system_user_page
                .open("/view-oebs-agreement-detail.xhtml")
                .check_title("Показ детальных условий договоров");
        
        table.reset_table_state();
        //Filtering
        table
                .open_filter()
                .set_value("Ид заказчика",
                           table.get_cell(1, "Ид заказчика").getText())
                .set_value("Ид А2",
                           table.get_cell(1, "Ид А2").getText())
                .set_value("Ид услуги",
                           table.get_cell(1, "Ид услуги").getText())
                .submit();
        table.check_table_should_have_size(1);
        table.reset_table_state();
        
        //Sortind
        table
                .open_sort()
                .set_value("Ид А2", SortingOptions.ASCENDING)
                .submit();
        table.check_table_column_sorted("Ид А2", SortingOptions.ASCENDING);
        table.reset_table_state();
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид договора")
    void T2_255_step_3() {
        system_user_page
                .open("/view-oebs-agreement-detail.xhtml")
                .check_title("Показ детальных условий договоров");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Детали договора");
    }
    
    @Step("Пользователь в \"Детали договора\" нажимает на А2/А3")
    void T2_255_step_4() {
        system_user_page
                .open("/view-oebs-agreement-detail.xhtml")
                .check_title("Показ детальных условий договоров");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Детали договора");
        // TODO: 018 18.08.18 Не реализовать дальше. На момент написания вылезает ШМЯК
    }
    
    @Step("Пользователь нажимает \"Экспорт в Excel\"\n" +
                  "\n" +
                  "Для удобства проверки данного функционала, необходимо отфильтровать таблицу и сделать объем информации меньше для быстрой выгрузки")
    void T2_255_step_5() {
        table.reset_table_state();
        table
                .open_filter()
                .set_value("Ид заказчика",
                           table.get_cell(1, "Ид заказчика").getText())
                .set_value("Ид А2",
                           table.get_cell(1, "Ид А2").getText())
                .set_value("Ид услуги",
                           table.get_cell(1, "Ид услуги").getText())
                .submit();
        // TODO: 018 18.08.18 На момент написание теста ничего не скачивается
        table.export_to_excel_download();
        table.reset_table_state();
    }
    
    @Step("Пользователь открывает страницу \"Связь договора с продуктом\"\n" + "\n"
                  + "Столбцы в таблице: Ид, Ид связи договора с продуктом, Ид договора, Ид продукта, Ид производителя, Модель, Серийный номер, Дата начала действия связи договора с продуктом, Дата окончания действия связи договора с продуктом, Статус.")
    void T2_231_step_1() {
        system_user_page
                .open("/view-oebs-agreement-link-to-oebs-product.xhtml")
                .check_title("Связь договора с продуктом");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания",
                                 "Ид связи договора с продуктом", "Ид договора", "Ид продукта",
                                 "Ид производителя", "Модель", "Серийный номер",
                                 "Дата начала действия связи договора с продуктом",
                                 "Дата окончания действия связи договора с продуктом");
        
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs связь с А2");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_231_step_LVE1() {
        system_user_page
                .open("/view-oebs-agreement-link-to-oebs-product.xhtml")
                .check_title("Связь договора с продуктом");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
    
    @Step("Пользователь открыввает страницу \"Показ детальных условий договоров с А3\"")
    void T2_256_step_1() {
        system_user_page
                .open("/view-oebs-agreement-detail-with-a3.xhtml")
                .check_title("Показ детальных условий договоров с А3");
    }
    
    @Step("Проверить работу фильтра по всем параметрам, сброс и отключение фильтра. Использование сортировки. Вернуть состояние в исходное. Кнопка \"Поделиться\" и \"Столбцы\"")
    void T2_256_step_2() {
        system_user_page
                .open("/view-oebs-agreement-detail-with-a3.xhtml")
                .check_title("Показ детальных условий договоров с А3");
        
        table.reset_table_state();
        
        //Filtering
        table
                .open_filter()
                .set_value("Ид заказчика",
                           table.get_cell(1, "Ид заказчика").getText())
                .set_value("Ид А2",
                           table.get_cell(1, "Ид А2").getText())
                .set_value("Ид услуги",
                           table.get_cell(1, "Ид услуги").getText())
                .submit();
        table.check_table_should_have_size(1);
        table.reset_table_state();
        
        //Sorting
        table
                .open_sort()
                .set_value("Ид А2", SortingOptions.ASCENDING)
                .submit();
        table.check_table_column_sorted("Ид А2", SortingOptions.ASCENDING);
        table.reset_table_state();
        
    }
    
    @Step("Пользователь нажимает \"Экспорт в Excel\"\n" +
                  "\n" +
                  "Для удобства проверки данного функционала, необходимо отфильтровать таблицу и сделать объем информации меньше для быстрой выгрузки")
    void T2_256_step_3() {
//        system_user_page
//                .open("/view-oebs-agreement-detail-with-a3.xhtml")
//                .check_title("Показ детальных условий договоров с А3");
//
//        table.reset_table_state();
//
//        //Фильтрация для уменьшения строк перед экспортом в Excel
//        table
//                .open_filter()
//                .set_value("Ид заказчика",
//                           table.get_cell(1, "Ид заказчика").getText())
//                .set_value("Ид А2",
//                           table.get_cell(1, "Ид А2").getText())
//                .set_value("Ид услуги",
//                           table.get_cell(1, "Ид услуги").getText())
//                .submit();
//        table.export_to_excel_download();
        ApplicationManager.blocked_dummy("При нажатии экспорт в Excel ничего не происходит");
    }
    
    @Step("Пользователь выбирает запись в таблице и нажимает на Ид договора ")
    void T2_256_step_4() {
        system_user_page
                .open("/view-oebs-agreement-detail-with-a3.xhtml")
                .check_title("Показ детальных условий договоров с А3");
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Детали договора");
        // TODO: 018 18.08.18 Не реализовать дальше. На момент написания вылезает ШМЯК
    }
    
    @Step("Пользователь в \"Детали договора\" нажимает на А2/А3")
    void T2_256_step_5() {
    }
    
    @Step("Пользователь открывает страницу \"Связь договора с услугой и продуктом\"\n" + "\n"
                  + "Столбцы в таблице: Ид, Ид связи договора с услугой, Ид связи договора с продуктом, Ид договора, Ид услуги, Дата начала действия связи договора с услугой и продуктом, Дата окончания действия связи договора с услугой и продуктом, Статус.")
    void T2_232_step_1() {
        system_user_page
                .open("/view-oebs-agreement-link-to-oebs-service.xhtml")
                .check_title("Связь договора с услугой и продуктом");
        table.check_headers_text("Ид", "Посл. изм.", "Статус", "Дата создания",
                                 "Ид связи договора с услугой", "Ид связи договора с продуктом",
                                 "Ид договора", "Ид услуги",
                                 "Дата начала действия связи договора с услугой и продуктом",
                                 "Дата окончания действия связи договора с услугой и продуктом");
        
        table.get_cell(1, "Ид").click();
        system_user_page.check_title("Oebs связь с сервисом");
    }
    
    @Step("Проверить фильтр, сортировку, обновление, поделиться, переключение страниц, отображение страницы по количесву записей (20-50-100)")
    void T2_232_step_LVE1() {
        system_user_page
                .open("/view-oebs-agreement-link-to-oebs-service.xhtml")
                .check_title("Связь договора с услугой и продуктом");
        
        table.reset_table_state();
        table.standart_filtering_recipe();
        table.standart_sorting_recipe();
    }
}
