package test;

import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "12 Управление инженерами | Утверждение выполненных работ")
public class PerformedWorkApprovalTests extends PerformedWorkApprovalSteps {

    @Test(description = "T2-157:Акты выполненных работ")
    @TmsLink("1071")
    public void T2_157() {
        T2_157_step_1();
        T2_157_step_2();
    }

}
