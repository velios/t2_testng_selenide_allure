package test;

import cfg.Constants;
import com.codeborne.selenide.SelenideElement;
import core.ApplicationManager;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static core.ApplicationManager.return_right_element_by_cell_title;

public class EngineerSectionRootSteps extends EngineerTestsBase {
    
    @Step("На данный момент страница пустая")
    void T2_245_step_1() {
        engineer_page
                .open("/freelancer-dashboard.xhtml")
                .check_title("Рабочий стол инженера");
    }
    
    @Step("Пользователь открывает страницу \"Профиль инженера\"")
    void T2_54_step_1() {
        engineer_page
                .open("/freelancer-profile-fr.xhtml")
                .check_title("Профиль инженера");
    }
    
    @Step("Пользователь просматривает все вкладки профиля")
    void T2_54_step_2() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь открывает страницу \"Запросы на изминение профиля\"")
    void T2_42_step_1() {
        engineer_page
                .open("/view-change-person-requests-fr.xhtml")
                .check_title("Запросы на изменение профиля");
    }
    
    @Step("Пользователь выбирает запрос в таблице и нажимает на Ид")
    void T2_42_step_2() {
        table.get_cell(1, "Ид").click();
        system_user_page.check_h1_header_contains("Запрос на");
    }
    
    @Step("Пользователь нажимает \"Создать новый запрос\"")
    void T2_42_step_3() {
        app.not_implemented_dummy();
    }
    
    @Step("Пользователь открывает страницу \"Заказанные отчеты\"")
    void T2_40_step_1() {
        engineer_page
                .open("/view-jobs.xhtml")
                .check_title("Заказанные отчеты");
    }
    
    @Step("Пользователь нажимает \"Скачать\"")
    void T2_40_step_2() {
        app.blocked_dummy("Недоступна кнопка скачать для инженера");
    }
    
    @Step("Пользователь через меню нажимает на \"Вопросы и ответы\"")
    void T2_208_step_1() {
        engineer_page.open_start_page();
        menu
                .download_item("Вопросы и ответы")
                .check_md5_hash("1c5240ac2c643f33ed69e875c0507c12");
    }
    
    @Step("Пользователь открывает страницу \"Изменить пароль\"")
    void T2_43_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
    }
    
    @Step("Пользователь вводит текущий пароль, новый пароль,подтвердить пароль строго по правилам: пароль не менее 6 символов, содержащий латинские буквы, цифры, минимум 1 букву в верхнем регистре и одну букву в нижнем регистре. Нажимает \"Сохранить\"")
    void T2_43_step_2() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        String VALID_PASSWORD = "Qwerty1234567890";
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue(VALID_PASSWORD);
        password_confirm.setValue(VALID_PASSWORD);
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Пароль успешно изменен")
                .check_is_info();

//        Вернуть старый пароль
        
        password_current.setValue(VALID_PASSWORD);
        password_new.setValue(cfg.external_user_password());
        password_confirm.setValue(cfg.external_user_password());
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Пароль успешно изменен")
                .check_is_info();
    }
    
    @Step("Пользователь вводит одинаковые пароли в текущий и новый пароль. Нажимает \"Сохранить\"")
    void T2_44_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue(cfg.external_user_password());
        password_confirm.setValue(cfg.external_user_password());
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Новый пароль должен отличаться от существующего")
                .check_is_error();
    }
    
    @Step("Пользователь вводит любой другой пароль в значение “Текущий пароль”. Нажимает \"Сохранить\"")
    void T2_45_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue("Wrong_password");
        password_new.setValue("Right_password_12345");
        password_confirm.setValue("Right_password_12345");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Пароль неверен")
                .check_is_error();
    }
    
    @Step("Пользователь вводит разные пароли в значение “новый пароль” и “Подтвердите пароль” Нажимает \"Сохранить\"")
    void T2_46_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue("Right_password_12345");
        password_confirm.setValue("Different_password");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Пароли не совпадают")
                .check_is_error();
    }
    
    @Step("Пользователь оставляет поля пустыми и нажимает \"Сохранить\"")
    void T2_47_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Текущий пароль: значение обязательно")
                .check_is_error();
        popup
                .with_message_contains("Новый пароль: значение обязательно")
                .check_is_error();
    }
    
    @Step("Пользователь вводит пароль маленькики буквами")
    void T2_49_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue("smallletters");
        password_confirm.setValue("smallletters");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Введенный пароль не соответствует требованиям политики безопасности")
                .check_is_error();
    }
    
    @Step("Пользователь вводит пароль менее 6 символов")
    void T2_48_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue("short");
        password_confirm.setValue("short");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Введенный пароль не соответствует требованиям политики безопасности")
                .check_is_error();
    }
    
    @Step("Пользователь вводит пароль только заглавными буквами")
    void T2_50_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue("CAPSPASSWORD");
        password_confirm.setValue("CAPSPASSWORD");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Введенный пароль не соответствует требованиям политики безопасности")
                .check_is_error();
    }
    
    @Step("Пользователь вводит пароль только цифрами")
    void T2_51_step_1() {
        engineer_page
                .open("/change-password.xhtml")
                .check_title("Изменить пароль");
        
        SelenideElement password_current = return_right_element_by_cell_title("Текущий пароль:");
        SelenideElement password_new = return_right_element_by_cell_title("Новый пароль:");
        SelenideElement password_confirm = return_right_element_by_cell_title("Подтвердите пароль:");
        
        password_current.setValue(cfg.external_user_password());
        password_new.setValue("1234567890");
        password_confirm.setValue("1234567890");
        
        $(byText("Сохранить")).click();
        
        popup
                .with_message_contains("Введенный пароль не соответствует требованиям политики безопасности")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Акты выполненных работ\"")
    void T2_35_step_1() {
        engineer_page
                .open("/view-works-completed.xhtml")
                .check_title("Акты выполненных работ");
    }
    
    @Step("Пользователь нажимает \"Выставить акты\"")
    void T2_35_step_2() {
        $("#form").find(byText("Выставить акты")).click();
        $(byText("Получить код по SMS")).shouldBe(visible);
    }
    
    @Step("Пользователь выбирает \"выставить все акты\" или нет, для подтверждени явводит код, который нужно получить по SMS. Нажимает \"Выставить акты\"")
    void T2_35_step_3() {
        app.blocked_dummy("Нет возможности выставлять себе акты и подтверждать их по смс");
    }
    
    @Step("Польователь оставляет поля. Нажимает \"Выставить акты\"")
    void T2_36_step_1() {
        engineer_page
                .open("/view-works-completed.xhtml")
                .check_title("Акты выполненных работ");
        
        $("#form\\:j_idt61").click();
        $("#form\\:j_idt147").click();
//        $(byText("Выставить акты")).click();
//        $(byText("Выставить акты")).click();
        
        popup
                .with_message_contains("Необходимо сгенерировать код")
                .check_is_error();
    }
    
    @Step("Пользователь открывает страницу \"Мои сообщения\"")
    void T2_53_step_1() {
        engineer_page
                .open("/view-external-user-notifications.xhtml")
                .check_title("Мои сообщения");
    }
    
    @Step("Пользователь открывает страницу \"Розыгрыш новой заявки\"")
    void T2_56_step_1() {
        engineer_page
                .open("/accept-incident.xhtml")
                .check_title("Розыгрыш новой заявки");
    }
    
    @Step("Пользователь выбирает заявку в таблице и нажимает на Ид")
    void T2_56_step_2() {
        app.blocked_dummy(Constants.DUMMY_NOT_DATA_IN_TABLE);
    }
    
    @Step("Пользователь нажимает на согласие с розыгыршем. Нажимает \"Принять участие в розыгрыще\"")
    void T2_56_step_3() {
    }
    
    @Step("Пользователь открывает страницу \"Запрос на регистрацию инженера\"")
    void T2_169_step_1() {
        app.not_implemented_dummy();
        
    }
    
    @Step("Пользователь в разделе общие данные заполняет: пол и и нажимает \"Изменить\" телефон для SMS умедомлений.")
    void T2_169_step_2() {
    }
    
    @Step("Пользователь вводит номер телефона и нажимает получить код")
    void T2_169_step_3() {
    }
    
    @Step("Пользователь вводит код и нажимает \"Изменить\"")
    void T2_169_step_4() {
    }
    
    @Step("Пользователь переходит во вкладку \"Сведения о рождении\" и вводит: дату рождения, страна, область, район, город. Нажимает \"Сохранить\"")
    void T2_169_step_5() {
    }
    
    @Step("Пользователь переходит во вкладку \"Тестирование\" и нажимает \"Добавить запись\" и выбирает необходимый навык и нажимает \"Добавить\"")
    void T2_169_step_6() {
    }
    
    @Step("Пользователь выбирает в таблице навык который он добавил в таблицу и нажимает \"Удалить запись\"")
    void T2_169_step_7() {
    }
    
    @Step("Пользователь нажимает \"Зарегистрировать запрос\"")
    void T2_169_step_8() {
    }
    
    @Step("Пользователь открывает страницу \"Заказ-наряды\"")
    void T2_280_step_1() {
        engineer_page
                .open("/view-oebs-work-orders-by-engineer.xhtml")
                .check_title("Заказ-наряды");
    }
    
    @Step("Проверить работу фильтра, сброса фильтра и отключения. Работу сортировки, вернуть состояние в исходное, поделиться, столбцы, переключение страниц.")
    void T2_280_step_2() {
        ApplicationManager.blocked_dummy(Constants.DUMMY_NOT_DATA_IN_TABLE);
    }
    
    @Step("Пользователь выбирает заказ-наряд и нажимает на Ид")
    void T2_280_step_3() {
    }
}
