package test;

import io.qameta.allure.Issue;
import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "23 НСИ | Oebs справочники")
public class OebsCatalogsTests extends OebsCatalogsSteps {
    
    @Test(description = "T2-249:Справочник А3")
    @TmsLink("1111")
    public void T2_249() {
        T2_249_step_1();
        T2_249_step_2();
        T2_249_step_3();
    }
    
    @Test(description = "T2-250:Справочник A2")
    @TmsLink("1112")
    public void T2_250() {
        T2_250_step_1();
        T2_250_step_2();
        T2_250_step_3();
    }
    
    @Test(description = "T2-221:Справочник сервисов")
    @TmsLink("1113")
    public void T2_221() {
        T2_221_step_1();
        T2_221_step_2();
        T2_221_step_LVE1();
    }
    
    @Test(description = "T2-222:Справочник продуктов")
    @TmsLink("1114")
    public void T2_222() {
        T2_222_step_1();
        T2_222_step_LVE1();
    }
    
    @Test(description = "T2-223:Справочник производителей")
    @TmsLink("1115")
    public void T2_223() {
        T2_223_step_1();
        T2_223_step_LVE1();
    }
    
    @Test(description = "T2-224:Справочник моделей")
    @TmsLink("1116")
    public void T2_224() {
        T2_224_step_1();
        T2_224_step_LVE1();
    }
    
    @Test(description = "T2-225:Справочник заказчиков")
    @TmsLink("1117")
    public void T2_225() {
        T2_225_step_1();
        T2_225_step_LVE1();
    }
    
    @Test(description = "T2-226:Справочник проектов")
    @TmsLink("1118")
    public void T2_226() {
        T2_226_step_1();
        T2_226_step_2();
        T2_226_step_LVE1();
    }
    
    @Test(description = "T2-227:Справочник организаций")
    @TmsLink("1119")
    public void T2_227() {
        T2_227_step_1();
        T2_227_step_LVE1();
    }
    
    @Test(description = "T2-228:Справочник зон")
    @TmsLink("1120")
    public void T2_228() {
        T2_228_step_1();
        T2_228_step_LVE1();
    }
    
    @Test(description = "T2-229:Справочник договоров (Договоры)")
    @TmsLink("1121")
    public void T2_229() {
        T2_229_step_1();
        T2_229_step_LVE1();
    }
    
    @Test(description = "T2_255:Показ детальных условий договоров")
    @TmsLink("1199")
    @Issue("D-640")
    public void T2_255() {
        T2_255_step_1();
        T2_255_step_2();
        T2_255_step_3();
        T2_255_step_4();
        T2_255_step_5();
    }
    
    @Test(description = "T2-231:Связь договора с продуктом")
    @TmsLink("1123")
    public void T2_231() {
        T2_231_step_1();
        T2_231_step_LVE1();
    }
    
    @Test(description = "T2_256:Показ детальных условий договоров с А3 ")
    @TmsLink("1200")
    public void T2_256() {
        T2_256_step_1();
        T2_256_step_2();
        T2_256_step_3();
        T2_256_step_4();
        T2_256_step_5();
    }
    
    @Test(description = "T2-232:Связь договора с услугой и продуктом")
    @TmsLink("1124")
    public void T2_232() {
        T2_232_step_1();
        T2_232_step_LVE1();
    }
}
