package test;

import io.qameta.allure.TmsLink;
import org.testng.annotations.Test;

@Test(testName = "223 НСИ | Обработка данных | Требования компетенций")
public class CompetencyRequirementsTests extends CompetencyRequirementsSteps {
    
    @Test(description = "T2-217:Приоритеты")
    @TmsLink("1107")
    public void T2_217() {
        T2_217_step_1();
        T2_217_step_2();
        T2_217_step_3();
        T2_217_step_4();
        T2_217_step_5();
        T2_217_step_6();
        T2_217_step_7();
    }
    
    @Test(description = "T2-218:Правила")
    @TmsLink("1108")
    public void T2_218() {
        T2_218_step_1();
        T2_218_step_2();
        T2_218_step_3();
        T2_218_step_4();
        T2_218_step_5();
        T2_218_step_6();
        T2_218_step_7();
    }
    
    @Test(description = "T2-219:Фазы")
    @TmsLink("1109")
    public void T2_219() {
        T2_219_step_1();
        T2_219_step_2();
        T2_219_step_3();
        T2_219_step_4();
        T2_219_step_5();
    }
    
    @Test(description = "T2-220:Проверка требуемых компетенций")
    @TmsLink("1110")
    public void T2_220() {
        T2_220_step_1();
        T2_220_step_2();
    }
}
